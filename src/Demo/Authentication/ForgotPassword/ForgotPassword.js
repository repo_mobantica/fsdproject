import React from 'react';
import {NavLink} from 'react-router-dom';
import {Redirect} from 'react-router-dom';

import './../../../assets/scss/style.scss';
import Aux from "../../../hoc/_Aux";
import Breadcrumb from "../../../App/layout/AdminLayout/Breadcrumb";
import {PostData} from '../../../services/PostData';

class ForgotPassword extends React.Component {
    constructor(){
        super();       
        this.state = {
            emailId: '',
         redirectToReferrer: false,
         forgotErr:'',
         emailIdErr:'',
         forgotSucc:''
        };
    
        this.forgotPassword = this.forgotPassword.bind(this);
        this.onChange = this.onChange.bind(this);
      }
      forgotPassword() {
        if(this.state.emailId){
            console.log("test");
          PostData('forgotPassword',this.state).then((result) => {
           let responseJson = result;
           if(responseJson.userData){         
             //sessionStorage.setItem('userData',JSON.stringify(responseJson));
             this.setState({forgotSucc:"Resset password link send successfully on email!"});
             setTimeout(() => {
                //this.setState({redirectToReferrer: true});
              }, 1000);
             
           }else{
            this.setState({forgotErr:"Please enter valid credentils!"});
           }
           
          });
        }else{
            if (this.state.emailId === '') {
                this.setState({emailIdErr:"Please enter email id!"});
            }
        }
       }
      onChange(e){
        this.setState({[e.target.name]:e.target.value});
       }
    render () {
        if (this.state.redirectToReferrer) {
            return (<Redirect to={"/auth/signin-1"}/>)
          }
          if(sessionStorage.getItem('userData')){
            //return (<Redirect to={'/dashboard/default'}/>)
          }
        return(
            <Aux>
                <Breadcrumb/>
                <div className="auth-wrapper">
                    <div className="auth-content">
                        <div className="auth-bg">
                            <span className="r"/>
                            <span className="r s"/>
                            <span className="r s"/>
                            <span className="r"/>
                        </div>
                        <div className="card">
                            <div className="card-body text-center">
                                <div className="mb-4">
                                    <i className="feather icon-mail auth-icon"/>
                                </div>
                                <h3 className="mb-4">Reset Password</h3>
                                <div className="input-group mb-3">
                                    <input type="email" className="form-control" placeholder="Email" name="emailId" onChange={this.onChange}/>
                                </div>
                                <p className="text-danger">{this.state.emailIdErr}</p>
                                <input type="submit" className="btn btn-primary shadow-2 mb-4" onClick={this.forgotPassword} name="reset-password" value="Reset Password"/>
                                <p className="text-danger">{this.state.forgotErr}</p>
                                <p className="text-success">{this.state.forgotSucc}</p>
                                <p className="mb-2 text-muted">Go to <NavLink to="/auth/signin-1">Login</NavLink></p>
                                {/* <p className="mb-0 text-muted">Don’t have an account? <NavLink to="/auth/signup-1">Signup</NavLink></p>*/}
                            </div>
                        </div>
                    </div>
                </div>
            </Aux>
        );
    }
}

export default ForgotPassword;