import React from 'react';
import {NavLink} from 'react-router-dom';
import {Redirect} from 'react-router-dom';

import './../../../assets/scss/style.scss';
import Aux from "../../../hoc/_Aux";
import Breadcrumb from "../../../App/layout/AdminLayout/Breadcrumb";
import {PostData} from '../../../services/PostData';

class SignUp1 extends React.Component {
    constructor(){
        super();       
        this.state = {
            emailId: '',
            password: '',
            redirectToReferrer: false,
            loginErr:'',
            emailIdErr:'',
            passwordErr:'',
            loginSucc:''
        };
    
        this.login = this.login.bind(this);
        this.onChange = this.onChange.bind(this);
      }
      login() {
        if(this.state.emailId && this.state.password){
        //     console.log("test");
        //   PostData('login',this.state).then((result) => {
        //    let responseJson = result;
        //    if(responseJson.userData){         
        //     //  sessionStorage.setItem('userData',JSON.stringify(responseJson));
        //      this.setState({loginSucc:"Login successfully done!"});
        //      setTimeout(() => {
                this.setState({redirectToReferrer: true});
        //       }, 1000);
             
        //    }else{
        //     this.setState({loginErr:"Please enter valid credentils!"});
        //    }
           
        //   });
        }else{
            if (this.state.emailId === '') {
                this.setState({emailIdErr:"Please enter email id!"});
            }

            if (this.state.password === '') {
                this.setState({passwordErr:"Please enter password!"});
            }
        }
       }
      onChange(e){
        this.setState({[e.target.name]:e.target.value});
       }
       
    render () {
        if (this.state.redirectToReferrer) {
            return (<Redirect to={"/dashboard/default"}/>)
          }
          if(sessionStorage.getItem('userData')){
            return (<Redirect to={'/dashboard/default'}/>)
          }
        return(
            <Aux>
                <Breadcrumb/>
                <div className="auth-wrapper">
                    <div className="auth-content">
                        <div className="auth-bg">
                            <span className="r"/>
                            <span className="r s"/>
                            <span className="r s"/>
                            <span className="r"/>
                        </div>
                        <div className="card">
                            <div className="card-body text-center">
                                <div className="mb-4">
                                    <i className="feather icon-unlock auth-icon"/>
                                </div>
                                <h3 className="mb-4">Login</h3>
                                <div className="input-group mb-3">
                                    <input type="email" className="form-control" placeholder="Email" name="emailId" onChange={this.onChange}/>
                                </div>
                                <p className="text-danger">{this.state.emailIdErr}</p>
                                <div className="input-group mb-4">
                                    <input type="password" className="form-control" placeholder="password" name="password" onChange={this.onChange}/>
                                </div>
                                <p className="text-danger">{this.state.passwordErr}</p>
                                {/*<div className="form-group text-left">
                                    <div className="checkbox checkbox-fill d-inline">
                                        <input type="checkbox" name="checkbox-fill-1" id="checkbox-fill-a1" />
                                            <label htmlFor="checkbox-fill-a1" className="cr"> Save credentials</label>
                                    </div>
        </div>*/}
                                <input type="submit" className="btn btn-primary shadow-2 mb-4" onClick={this.login} name="login" value="Login"/>
                                <p className="text-danger">{this.state.loginErr}</p>
                                <p className="text-success">{this.state.loginSucc}</p>
                                <p className="mb-2 text-muted">Forgot password? <NavLink to="/auth/forgotpassword">Reset</NavLink></p>
                                {/*<p className="mb-0 text-muted">Don’t have an account? <NavLink to="/auth/signup-1">Signup</NavLink></p>*/}
                            </div>
                        </div>
                    </div>
                </div>
            </Aux>
        );
    }
}

export default SignUp1;