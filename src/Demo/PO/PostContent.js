import React from 'react';
import moment from 'moment';

const PostContent = (props) => (
  <div className="uk-card uk-card-default uk-card-small uk-card-body uk-border-rounded custom-border uk-margin" >
    <div className="uk-grid uk-grid-medium uk-flex uk-flex-middle" data-uk-grid>
      <div className="uk-width-1-1">
        <h4 className="uk-card-title uk-margin-remove-top uk-margin-remove-bottom">
          <a className="uk-link-reset uk-text-bold" href={props.post.url}>
            {props.post.title}
          </a>
        </h4>
        <span className="uk-article-meta">
          <span data-uk-icon="calendar"></span>
           Published at {moment(props.post.created_at).format('lll')}
        </span>
      </div>
      <div className="uk-width-1-3@s uk-width-2-6@m uk-height-1-1 uk-margin-small-top">
         <img data-src={props.post.image} alt={props.post.title } data-uk-img />
      </div>
      <div className="uk-width-2-3@s uk-width-4-6@m uk-margin-small-top">
        <p className="uk-margin-small uk-text-justify">
          {props.post.body}
        </p>
        <a data-uk-icon="icon:  arrow-right" href={props.post.url} title="Read More" 
        className="uk-button uk-button-link uk-button-small uk-text-bold">Read More</a>           
      </div>
    </div>
  </div>
);

export default PostContent;