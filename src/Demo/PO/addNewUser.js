import React from 'react';
import {Row, Col, Card, Form, Button, InputGroup, FormControl, DropdownButton, Dropdown } from 'react-bootstrap';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import Aux from "../../hoc/_Aux";
import {PostData} from '../../services/PostData';
import { Multiselect } from 'multiselect-react-dropdown';
import {GetData} from '../../services/GetData';

class AddUser extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            firstName : "",
            lastName : "",
            emailId : "",
            roleType : [],
            status : "",
            menuOptions : []
        };
        this.adduser = this.adduser.bind(this);
        this.onChange = this.onChange.bind(this);
        this.onRemove = this.onRemove.bind(this);
        this.onSelect = this.onSelect.bind(this);
        
    }
    
    adduser = () => {
        // if(this.state.username && this.state.password && this.state.email && this.state.name){
            // const data = JSON.stringify({
            //     firstName : this.state.firstName,
            //     lastName : this.state.lastName,
            //     emailId : this.state.emailId,
            //     roleType : this.state.roleType,
            //     status : this.state.status
            // })
        // PostData('adduser',this.state).then((result) => {  
        //   let responseJson = result;
        //   if(responseJson.userData){         
        //     sessionStorage.setItem('userData',JSON.stringify(responseJson));
        //     this.setState({redirectToReferrer: true});
        //   }

         
            fetch('http://127.0.0.1:8000/api/test/')
            .then(function(response){
                response.json().then(function(resp){
                console.log(resp);
            });        
        
        });
          
        }
    //   }

    onChange = (e) => {
        this.setState({[e.target.name]:e.target.value});
    }

    componentDidMount() {
        GetData('roletype',this.state).then((result) => {
            let responseJson = result; 
            this.setState({roleType:responseJson.data});
           });
        GetData('menu',this.state).then((result) => {
            let responseJson = result; 
            this.setState({menuOptions:responseJson.data});
           });
    }

    onSelect(item) {
        // const { selectedValues } = this.state;
        // const { selectionLimit, onSelect, singleSelect, showCheckbox } = this.props;
        // this.setState({
        //   inputValue: ''
        // });
        // if (singleSelect) {
        //   this.onSingleSelect(item);
        //   onSelect([item], item);
        //   return;
        // }
        // if (this.isSelectedValue(item)) {
        //   this.onRemoveSelectedItem(item);
        //   return;
        // }
        // if (selectionLimit == selectedValues.length) {
        //   return;
        // }
        //     selectedValues.push(item);
        //     onSelect(selectedValues, item);
        // this.setState({ selectedValues }, () => {
        //   if (!showCheckbox) {
        //             this.removeSelectedValuesFromOptions(true);
        //   }
        // });
        // if (!this.props.closeOnSelect) {
        //   this.searchBox.current.focus();
        // }
      }
     
    onRemove(item) {
    //     let { selectedValues, index = 0, isObject } = this.state;
	// 	const { onRemove, showCheckbox } = this.props;
    // if (isObject) {
    //   index = selectedValues.findIndex(
    //     i => i[displayValue] === item[displayValue]
    //   );
    // } else {
    //   index = selectedValues.indexOf(item);
    // }
	// 	selectedValues.splice(index, 1);
	// 	onRemove(selectedValues, item);
    // this.setState({ selectedValues }, () => {
    //   if (!showCheckbox) {
	// 			this.removeSelectedValuesFromOptions(true);
    //   }
    // });
    }

    render() {
 console.log("STTSTS", this.state)
        return (
            <Aux>
                <Row>
                    <Col>
                        <Card>
                            {/* <Card.Header>
                                <Card.Title as="h5">PO Management List</Card.Title>
                            </Card.Header> */}
                            <Card.Body>
                                {/* <h5>Form controls</h5> */}
                                <hr/>
                                <Row>
                                
                                    <Col md={6}>
                                    <Form>
                                        <Form.Group controlId="exampleForm.ControlInput1">
                                            <Form.Label>First Name</Form.Label>
                                            <Form.Control type="text" placeholder="Enter First Name" name="firstName" onChange={this.onChange} value={this.state.firstName}/>
                                        </Form.Group>
                                        <Form.Group controlId="exampleForm.ControlInput2">
                                            <Form.Label>Email ID</Form.Label>
                                            <Form.Control type="text" placeholder="Enter Email ID" name="emailId" onChange={this.onChange} value={this.state.emailId}/>
                                        </Form.Group>
                                        <Form.Label>Status</Form.Label>
                                        <Form.Control size="small" as="select" className="mb-3" name="status" onChange={this.onChange} value={this.state.status}>
                                            <option>Please select</option>
                                            <option>Active</option>
                                            <option>Inactive</option>
                                        </Form.Control>
                                     </Form>
                                     </Col>
                                    <Col md={6}>
                                        <Form.Group controlId="exampleForm.ControlInpu3">
                                            <Form.Label>Last Name</Form.Label>
                                            <Form.Control type="text" placeholder="Enter Last Name" onChange={this.onChange} name="lastName" value={this.state.lastName}/>
                                        </Form.Group>
                                    
                                        <Form.Group controlId="exampleForm.ControlInput4">
                                            <Form.Label>Role Type</Form.Label>
                                       
                                            <Multiselect
                                                options={this.state.roleType} // Options to display in the dropdown
                                                selectedValues={this.state.selectedValue} // Preselected value to persist in dropdown
                                                onSelect={this.onSelect} // Function will trigger on select event
                                                onRemove={this.onRemove} // Function will trigger on remove event
                                                displayValue="role" // Property name to display in the dropdown options
                                                />
                                         </Form.Group>

                                         <Form.Group controlId="exampleForm.ControlInput4">
                                            <Form.Label>Permissions</Form.Label>
                                       
                                            <Multiselect
                                                options={this.state.menuOptions} // Options to display in the dropdown
                                                selectedValues={this.state.selectedValue} // Preselected value to persist in dropdown
                                                onSelect={this.onSelect} // Function will trigger on select event
                                                onRemove={this.onRemove} // Function will trigger on remove event
                                                displayValue="name" // Property name to display in the dropdown options
                                                />
                                         </Form.Group>

                                    </Col>
                                   
                                </Row>
                                <Row>
                                <input type="submit" className="btn btn-primary" value="Sign Up" onClick={this.adduser}/>
                                </Row>
                            </Card.Body>
                            </Card>
                    </Col>
                </Row>
            </Aux>
        );
    }
}

export default AddUser;
