import React from 'react';
import {Row, Col, Card, Form, Button} from 'react-bootstrap';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import Aux from "../../hoc/_Aux";
import {GetData} from '../../services/GetData';

class FormsElements extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            commodity : "",
            orderFrom : "",
            itemType : "",
            patternType : "",
            poDeliveryTerm : "",
            paymentTerm : "",
            castingDraweringRevision : "",
            machiningDraweringRevision : "",
            remark : "",
            patternCost : "",
            commodityArray:[],
            selectedOption:"",
            orderfromArray:[],
            actualshippingArray : [],
            vendorinvArray : [],
            cepacertArray : [],
            setquadocArray : [],
            startDate: new Date()

        };
    }
    componentDidMount() {
        GetData('commodity',this.state).then((result) => {
            let responseJson = result;
            this.setState({commodityArray:responseJson.data});
           });
           GetData('orderfrom',this.state).then((result) => {
            let responseJson = result;
            this.setState({orderfromArray:responseJson.data});
           });
           GetData('actulashipping',this.state).then((result) => {
            let responseJson = result;
            this.setState({actualshippingArray:responseJson.data});
           });
           GetData('venderinvpacktype',this.state).then((result) => {
            let responseJson = result;
            this.setState({vendorinvArray:responseJson.data});
           });
           GetData('cepacertificate',this.state).then((result) => {
            let responseJson = result;
            this.setState({cepacertArray:responseJson.data});
           });
           GetData('setofquadoc',this.state).then((result) => {
            let responseJson = result;
            this.setState({setquadocArray:responseJson.data});
           });
    }
    handleChange = date => {
        this.setState({
          startDate: date
        });
    };
    render() {
 
        return (
            <Aux>
                <Row>
                    <Col>
                        <Card>
                            {/* <Card.Header>
                                <Card.Title as="h5">PO Management List</Card.Title>
                            </Card.Header> */}
                            <Card.Body>
                                {/* <h5>Form controls</h5> */}
                                <hr/>
                                <Row>
                                    <Col>
                                        <Form.Group controlId="formBasicEmail">
                                            <Form.Label>Vendor Name</Form.Label>
                                            <Form.Control type="text" placeholder="Enter vendor name" value="John" readOnly />
                                        </Form.Group> 
                                    </Col>
                                    <Col>
                                        <Form.Group controlId="formBasicEmail">
                                            <Form.Label>Commodity</Form.Label> 
                                            <Form.Control type="text" placeholder="Commodity" value="Test" readOnly />
                                        </Form.Group>
                                    </Col>
                                    <Col>
                                        <Form.Group controlId="formBasicEmail">
                                            <Form.Label>Order from</Form.Label>
                                            <Form.Control type="text" placeholder="Order From" value="Unknown" readOnly/>
                                        </Form.Group>
                                    </Col>
                                    <Col>
                                        <Form.Group controlId="formBasicEmail">
                                            <Form.Label>Project Name</Form.Label>
                                            <Form.Control type="text" placeholder="Enter project name" value="Test" readOnly/>
                                        </Form.Group>
                                    </Col>
                                    </Row>
                                    <Row>
                                        <Col>
                                        <Form.Group controlId="formBasicEmail">
                                            <Form.Label>Project Number</Form.Label>
                                            <Form.Control type="text" placeholder="Enter project Number" value="142" readOnly/>
                                        </Form.Group>
                                        </Col>
                                        <Col>
                                            <Form.Group controlId="exampleForm.ControlInput1">
                                                <Form.Label>PO Status</Form.Label>
                                                <Form.Control type="text" placeholder="PO status" value="approved" readOnly/>
                                            </Form.Group>
                                        </Col>
                                        <Col>
                                            <Form.Group controlId="formBasicEmail">
                                                <Form.Label>PO Number</Form.Label>
                                                <Form.Control type="text" placeholder="Enter Po number" value="1452" readOnly/>
                                            </Form.Group>
                                        </Col>
                                        <Col>
                                            <Form.Group controlId="exampleForm.ControlInput1">
                                                <Form.Label>PO Serial NO</Form.Label>
                                                <Form.Control type="text" placeholder="PO serial no" value="147" readOnly/>
                                            </Form.Group>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col>
                                            <Form.Group controlId="exampleForm.ControlInput1">
                                            <Form.Label>Po Date</Form.Label>
                                            <DatePicker
                                        className="form-control"
                                        value={"14/12/2018"}
                                        readOnly
                                        />
                                            </Form.Group>
                                        </Col>
                                        <Col>
                                            <Form.Group controlId="exampleForm.ControlInput1">
                                                <Form.Label>Pump Model</Form.Label>
                                                <Form.Control type="text" placeholder="Pump model" value="1245" readOnly/>
                                            </Form.Group>
                                        </Col>
                                        <Col>
                                            <Form.Group controlId="exampleForm.ControlInput1">
                                                <Form.Label>Item Description</Form.Label>
                                                <Form.Control type="textarea" placeholder="Item Description" value="Test" readOnly/>
                                            </Form.Group>
                                        </Col>
                                        <Col>
                                            <Form.Group controlId="exampleForm.ControlInput1">
                                                <Form.Label>Item Code</Form.Label>
                                                <Form.Control type="text" placeholder="Item Code" value="145" readOnly/>
                                            </Form.Group>
                                        </Col>
                                      
                                       
                                      
                                    </Row>
                                    <Row>
                                        <Col>
                                            <Form.Group controlId="exampleForm.ControlInput1">
                                                <Form.Label>Casting Drawering Number</Form.Label>
                                                <Form.Control type="number" placeholder="Casting Drawering Number" value="147" readOnly/>
                                            </Form.Group>
                                        </Col>
                                        <Col>
                                            <Form.Group controlId="exampleForm.ControlInput1">
                                                <Form.Label>Casting Drawering Revision</Form.Label>
                                                <Form.Control type="text" placeholder="Casting Drawering Revision" value="No" readOnly/>
                                            </Form.Group>
                                        </Col>
                                        <Col>
                                            <Form.Group controlId="exampleForm.ControlInput1">
                                                <Form.Label>Machining Drawering Number</Form.Label>
                                                <Form.Control type="number" placeholder="Machining Drawering Number" value="123" readOnly/>
                                            </Form.Group>
                                        </Col>
                                        <Col>
                                            <Form.Group controlId="exampleForm.ControlInput1">
                                                <Form.Label>Machining Drawering Revision</Form.Label>
                                                <Form.Control type="text" placeholder="Machining Drawering Revision" value="No" readOnly/>
                                            </Form.Group>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col>
                                            <Form.Label>Item Type</Form.Label>
                                            <Form.Control size="small" as="select" className="mb-3">
                                                <option>Please select</option>
                                                <option>New</option>
                                                <option>Repeat</option>
                                                <option>Other</option>
                                            </Form.Control>
                                        </Col>
                                        <Col>
                                            <Form.Group controlId="exampleForm.ControlInput1">
                                                <Form.Label>Material</Form.Label>
                                                <Form.Control type="number" placeholder="Material" value="147" readOnly/>
                                            </Form.Group>
                                        </Col>
                                        <Col>
                                            <Form.Group controlId="exampleForm.ControlInput1">
                                                <Form.Label>PO Quantity</Form.Label>
                                                <Form.Control type="number" placeholder="PO Quantity" value="140" readOnly/>
                                            </Form.Group>
                                        </Col>
                                        <Col>
                                            <Form.Group controlId="exampleForm.ControlInput1">
                                                <Form.Label>Dispatch Quantity</Form.Label>
                                                <Form.Control type="number" placeholder="Dispatch Quantity" value="140" readOnly/>
                                            </Form.Group>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col>
                                            <Form.Group controlId="exampleForm.ControlInput1">
                                                <Form.Label>Pending Quantity</Form.Label>
                                                <Form.Control type="number" placeholder="Pending Quantity" value="140" readOnly/>
                                            </Form.Group>
                                        </Col>
                                        <Col>
                                            <Form.Group controlId="formBasicEmail">
                                                <Form.Label>Vendor Invoice No</Form.Label>
                                                <Form.Control type="text" placeholder="Enter vendor Invoice No" value="1457" readOnly />
                                            </Form.Group>
                                        </Col>
                                        <Col>
                                            <Form.Group controlId="exampleForm.ControlInput1">
                                            <Form.Label>Vendor Invoice Date</Form.Label>
                                            <DatePicker className="form-control"
                                                value={"14/12/2018"}
                                                readOnly
                                                />
                                            </Form.Group>
                                            
                                        </Col>
                                        <Col>
                                        <Form.Group>
                                            <Form.Label>Actual Ex-workers Date</Form.Label>
                                            <DatePicker className="form-control"
                                                value={"14/12/2018"}
                                                readOnly
                                                />
                                                </Form.Group>
                                        </Col>
                                    </Row>
                                    


                                    <Row>
                                        <Col>
                                        <Form.Group controlId="exampleForm.ControlInput1">
                                        <Form.Label>ETD ( Port ) Date</Form.Label>
                                        <DatePicker className="form-control"
                                        onChange={this.handleChange}
                                        selected={this.state.startDate}
                                            />
                                        </Form.Group>
                                        </Col>
                                        <Col>
                                            <Form.Group controlId="exampleForm.ControlInput1">
                                                <Form.Label>Original Plan Date</Form.Label>
                                                <DatePicker className="form-control"
                                            onChange={this.handleChange}
                                            selected={this.state.startDate}
                                        />
                                            </Form.Group>
                                            
                                        </Col>
                                        <Col>
                                            <Form.Group controlId="exampleForm.ControlInput1">
                                                <Form.Label>Revised Date</Form.Label>
                                                <Form.Control size="small" as="select" className="mb-3">
                                            <option>14/12/2019</option>
                                            <option>14/1/2020</option>
                                            <option>14/1/2020</option>
                                        </Form.Control>
                                            </Form.Group>
                                            
                                        </Col>
                                        <Col>
                                            <Form.Group controlId="exampleForm.ControlInput1">
                                                <Form.Label>Result Date</Form.Label>
                                                <DatePicker className="form-control"
                                            onChange={this.handleChange}
                                            selected={this.state.startDate}
                                        />
                                            </Form.Group>
                                            
                                        </Col>
                                    </Row>



                                    <Row>
                                        <Col>
                                        <Form.Group controlId="exampleForm.ControlInput1">
                                        <Form.Label>Korea Reaching Date </Form.Label>
                                        <DatePicker className="form-control"
                                        onChange={this.handleChange}
                                            selected={this.state.startDate}
                                        />
                                        </Form.Group>
                                        </Col>
                                        <Col>
                                            <Form.Group controlId="exampleForm.ControlInput1">
                                                <Form.Label>Original Plan Date</Form.Label>
                                                <DatePicker className="form-control"
                                            onChange={this.handleChange}
                                            selected={this.state.startDate}
                                        />
                                            </Form.Group>
                                            
                                        </Col>
                                        <Col>
                                            <Form.Group controlId="exampleForm.ControlInput1">
                                                <Form.Label>Revised Date</Form.Label>
                                                <Form.Control size="small" as="select" className="mb-3">
                                            <option>14/12/2019</option>
                                            <option>14/1/2020</option>
                                            <option>14/1/2020</option>
                                        </Form.Control>
                                            </Form.Group>
                                            
                                        </Col>
                                        <Col>
                                            <Form.Group controlId="exampleForm.ControlInput1">
                                                <Form.Label>Result Date</Form.Label>
                                                <DatePicker className="form-control"
                                                    onChange={this.handleChange}
                                                    selected={this.state.startDate}
                                                />
                                            </Form.Group>
                                            
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col>
                                         
                                        <Form.Label>Actual Shipping Terms / Mode</Form.Label>
                                        <Form.Control size="small" as="select" className="mb-3">
                                        <option>Please select</option>
                                        {this.state.actualshippingArray.map((e, key) => {
                                            return <option key={key} value={e.id}>{e.value}</option>;
                                            })}
                                        
                                        </Form.Control> 
                                           
                                        </Col>
                                        <Col>
                                        <Form.Label>Vendor Invoice & Packing List</Form.Label>
                                        <Form.Control size="small" as="select" className="mb-3">
                                            <option>Please select</option>
                                            {this.state.vendorinvArray.map((e, key) => {
                                                return <option key={key} value={e.id}>{e.value}</option>;
                                                })}
                                            
                                        </Form.Control> 
                                        </Col>
                                        <Col md={3}>
                                        <Form.Group controlId="exampleForm.ControlInput1">
                                        <Form.Label>BL / AWB Date</Form.Label>
                                        <DatePicker className="form-control"
                                            onChange={this.handleChange}
                                            selected={this.state.startDate}
                                        />
                                    </Form.Group>
                                        </Col>
                                        <Col>
                                        <Form.Group controlId="exampleForm.ControlInput1">
                                        <Form.Label>BL / AWB Number</Form.Label>
                                        <Form.Control type="text" placeholder="BL / AWB Number" />
                                    </Form.Group>
                                            
                                        </Col>
                                    </Row>


                                    <Row>
                                        
                                        <Col md={3}>
                                            <Form.Label>CEPA Certificate</Form.Label>
                                            <Form.Control size="small" as="select" className="mb-3">
                                            <option>Please select</option>
                                            {this.state.cepacertArray.map((e, key) => {
                                                return <option key={key} value={e.id}>{e.value}</option>;
                                                })}
                                            
                                        </Form.Control> 
                                        </Col>
                                        <Col md={3}>
                                            <Form.Group controlId="exampleForm.ControlInput1">
                                                <Form.Label>Shipping Documents Sharing Date </Form.Label>
                                                <DatePicker className="form-control"
                                                    onChange={this.handleChange}
                                                    selected={this.state.startDate}
                                                />
                                                </Form.Group>
                                                
                                        </Col>
                                        <Col md={3}>
                                            <Form.Label>Set of Quality Documents</Form.Label>
                                            <Form.Control size="small" as="select" className="mb-3">
                                            <option>Please select</option>
                                            {this.state.setquadocArray.map((e, key) => {
                                                return <option key={key} value={e.id}>{e.value}</option>;
                                                })}
                                            
                                        </Form.Control> 
                                        </Col>
                                        <Col>
                                        </Col>
                                        <Col>
                                           
                                        </Col>
                                    </Row>

                                <Row>
                                    <Button variant="primary">
                                        Submit
                                    </Button>
                                </Row>
                                
                                
                            </Card.Body>
                        </Card>
                         
                    </Col>
                </Row>
            </Aux>
        );
    }
}

export default FormsElements;
