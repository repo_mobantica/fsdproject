import React, { Component } from 'react';
import {Row, Col, Card, Table, Button} from 'react-bootstrap';
import 'font-awesome/css/font-awesome.min.css';
import Aux from "../../hoc/_Aux";
import {GetData} from '../../services/GetData';
import  { NavLink , Redirect} from 'react-router-dom'
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
 
   class logistic extends Component{

	constructor(props){
        super(props);
        this.state = {
            show: false,
            data: [],
			referrer: ""

        };
        this.addDetails = this.addDetails.bind(this);
      
        
    }

    componentDidMount() {
        GetData('logistic',this.state).then((result) => {
            let responseJson = result;
			this.setState({data:responseJson.data});
           });
    }
    
    addDetails = () => {
        this.setState({referrer: './logisticDetails'});
          
        }
    //   }
 

    render() {
		const {referrer} = this.state;
        if (referrer) return <Redirect to={referrer} />;
        return (
            <Aux>
                <Row>
                    <Col>
                      <Card> 
                      <Row >
                            <Col>
							{/* <NavLink to="./logisticDetails"> Dashboard </NavLink> */}
                            {/* <input type="submit" className="btn btn-primary pull-right" value="Add" onClick={this.addDetails}  /> */}
                            </Col>
                        
                        </Row>
						<div className="table-responsive">
							<Table className="tat"> 
							<tr>
								<th>Id </th>
								<th>Vendor Name</th> 
								<th>Commodity</th>
								<th>Order From</th>
								<th>Project Name</th>
								<th>Project Number</th>
								<th>PO Status</th>
								<th>PO Number</th>
								<th>PO Serial No.</th>
								<th>Purchase Order Date </th>
								<th>Pump Model</th>
								<th>Item Description</th>
								<th>Item Code</th>
								<th>Casting Drawing Number</th>
								<th>Casting Drawing Revison</th>
								<th>Machining Drawing Number</th>
								<th>Machining Drawing Revison</th>
								<th>Item Type</th>
								<th>Material</th>
								<th>PO Quantity</th>
								<th>Dispatch Qty</th>
								<th>Pending Qty</th>
								<th>Vendor Invoice No.</th>
								<th>Vendor Invoice Date</th>
								<th>Actual Ex-works Date</th>
								<th>ETD ( Port ) Date</th>
								<th>Korea Reaching Date</th>
								<th>Actual Shipping Terms / Mode</th>
								<th>Vendor Invoice / Packing List</th>
								<th>BL / AWB Number</th>
								<th>BL / AWB Date</th>
								<th>CEPA Certificate</th>
								<th>Shipping Documents Sharing Date (With Korea Team)</th>
								<th>Set of Quality Documents</th>
								<th>Action</th>
							</tr>
							{
								this.state.data.map((dynamicData) =>
									<tr className="trow"> 
										<td>  {dynamicData.id} </td> 
										<td> {dynamicData.vendorName} </td>
										<td>{dynamicData.commodity}</td>
										<td>  {dynamicData.orderFrom} </td> 
										<td> {dynamicData.projectName} </td>
										<td>{dynamicData.projectNumber}</td>
										<td>  {dynamicData.poStatus} </td> 
										<td> {dynamicData.poNumber} </td>
										<td>{dynamicData.poSerialNo}</td>
										<td><DatePicker
                                        className="form-control"
                                        onChange={this.handleChange}
                                        selected={this.state.startDate}
                                        value=  {dynamicData.poDate}
                                        style = {{width : "auto"}} />  </td> 
										<td> {dynamicData.pumpModel} </td>
										<td>{dynamicData.itemDescription}</td>
										<td>  {dynamicData.itemCode} </td> 
										<td> {dynamicData.castingDrawingNo} </td>
										<td>{dynamicData.castingDrawingRevison}</td>
										<td>  {dynamicData.machiningDrawingNumber} </td> 
										<td> {dynamicData.machiningDrawingRevison} </td>
										<td>{dynamicData.itemType}</td>
										<td>  {dynamicData.material} </td> 
										<td> {dynamicData.poQuantity} </td>
										<td>{dynamicData.dispatchQty}</td>
										<td>  {dynamicData.pendingQty} </td> 
										<td> {dynamicData.vendorInvoiceNo} </td>
										<td> <DatePicker
                                        className="form-control"
                                        onChange={this.handleChange}
                                        selected={this.state.startDate}
                                        value= {dynamicData.vendorInvoiceDate}
                                         /> </td>
										<td><DatePicker
                                        className="form-control"
                                        onChange={this.handleChange}
                                        selected={this.state.startDate}
                                        value= {dynamicData.actualExworksDate}
                                         /></td>
										<td>   <DatePicker
                                        className="form-control"
                                        onChange={this.handleChange}
                                        selected={this.state.startDate}
                                        value=  {dynamicData.etdDate}
                                        style = {{width : "auto"}} /> </td> 
										<td> <DatePicker
                                        className="form-control"
                                        onChange={this.handleChange}
                                        selected={this.state.startDate}
                                        value=  {dynamicData.koreaReachingDate} 
                                        style = {{width : "auto"}} /></td>
										<td>{dynamicData.actualShippingTerms}</td>
										<td>  {dynamicData.vendorInvoicePacking} </td> 
										<td> {dynamicData.blAwbNo} </td>
										<td><DatePicker
                                        className="form-control"
                                        onChange={this.handleChange}
                                        selected={this.state.startDate}
                                        value= {dynamicData.blAwbDate}
                                        style = {{width : "auto"}} /></td>
										<td> {dynamicData.cepaCertificate} </td>
										<td><DatePicker
                                        className="form-control"
                                        onChange={this.handleChange}
                                        selected={this.state.startDate}
                                        value= {dynamicData.ShippingDocSharingDate}
                                        style = {{width : "auto"}} /></td>
										<td>{dynamicData.SetQualityDocs}</td>
										<td><i class="fa fa-eye" aria-hidden="true" style = {{ color : '#00CCCC' }} onClick={this.addDetails}></i> </td>
									</tr>
							) }
							</Table>
						</div>
                    </Card>
                    </Col>
                </Row>
				
            </Aux>
             
        );
    }
    
   }  
 
  export default logistic;

  
