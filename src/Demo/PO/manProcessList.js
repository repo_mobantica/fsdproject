import React, { Component } from 'react';
import {Row, Col, Card, Table, Button} from 'react-bootstrap';
import 'font-awesome/css/font-awesome.min.css';
import Aux from "../../hoc/_Aux";
import {GetData} from '../../services/GetData';
import  { NavLink , Redirect} from 'react-router-dom'
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
 
   class manProcess extends Component{

	constructor(props){
        super(props);
        this.state = {
            show: false,
            data: [],
			referrer: ""

        };
        this.addDetails = this.addDetails.bind(this);
      
        
    }

    componentDidMount() {
        GetData('manufacturingproc',this.state).then((result) => {
            let responseJson = result;
			this.setState({data:responseJson.data});
           });
    }
    
    addDetails = () => {
        this.setState({referrer: './manProcessDetails'});
          
        }
    //   }
 

    render() {
		const {referrer} = this.state;
        if (referrer) return <Redirect to={referrer} />;
        return (
            <Aux>
                <Row>
                    <Col>
                      <Card> 
                      {/* <Row >
                            <Col>
							
                            <input type="submit" className="btn btn-primary pull-right" value="Add" onClick={this.addDetails}  />
                            </Col>
                        
                        </Row> */}
						<div className="table-responsive">
							<Table striped bordered   className="tat"> 
                            <thead>
							<tr >
								<th>Id </th>
								<th>Vendor Name</th> 
								<th>Commodity</th>
								<th>Order From</th>
								<th>Project Name</th>
								<th>Project Number</th>
								<th>PO Status</th>
								<th>PO Number</th>
								<th>PO Serial No.</th>
                                <th>Item Type</th>
								<th>Purchase Date </th>
								<th>Pump Model</th>
								<th>Item Description</th>
								<th>Item Code</th>
								<th>Casting Drawing Number</th>
								<th>Casting Drawing Revison</th>
								<th>Machining Drawing Number</th>
								<th>Machining Drawing Revison</th>
                                <th>Special Quality Requirement</th>
								<th>Material</th>
								<th>PO Quantity</th>
                                <th>PO Delivery Term</th>
                                <th>PO Delivery Date</th>
                                <th>Vendor Commitement Date</th>
                                <th>Vendor Product Code</th>
                                <th>3D Model / 2D Drawing Clear Date</th>
                                <th>Pattern Date</th>
                                <th>Pouring Date</th>
                                <th>Casting / Raw Material Completion Date </th>
                                <th>Machining Date  </th>
                                <th>HGS Inspection Date</th>
                                <th>Packing & Ex Work Dispatch</th>
                                <th>ETD ( Port ) Date</th>
                                <th>Korea Reaching Date</th>
                                <th>Status</th>
                                <th>Dispatch Qty</th>
                                <th>Pending Qty</th>
                                <th>Vendor Invoice No.</th>
                                <th>Vendor Invoice Date</th>
                                <th>Actual Ex-works Date</th>
                                <th>Delay in Days</th>
                                <th>Delay Condition</th>
                                <th>Delay Reason</th>
								<th>Action</th>
							</tr>
                            </thead>
                            <tbody>
							{
                                this.state.data.map((dynamicData) =>
                                
									<tr className="trow"> 
										<td>  {dynamicData.id} </td> 
										<td> {dynamicData.vendorName} </td>
										<td>{dynamicData.commodity}</td>
										<td>  {dynamicData.orderFrom} </td> 
										<td> {dynamicData.projectName} </td>
										<td>{dynamicData.projectNumber}</td>
                                        <td>{dynamicData.poStatus}</td>
                                        <td> {dynamicData.poNumber} </td>
                                        <td>{dynamicData.poSerialNo}</td>
                                        <td>{dynamicData.itemType}</td>
								        <td width="200"> 
                                        <DatePicker
                                        className="form-control"
                                        onChange={this.handleChange}
                                        selected={this.state.startDate}
                                        value=  {dynamicData.poDate}
                                          />  </td> 
										<td> {dynamicData.pumpModel} </td>
										<td>{dynamicData.itemDescription}</td>
										<td>  {dynamicData.itemCode} </td> 
										<td> {dynamicData.castingDrawingNumber} </td>
										<td>{dynamicData.castingDrawingRevison}</td>
										<td>  {dynamicData.machiningDrawingNumber} </td> 
										<td> {dynamicData.machiningDrawingRevison} </td>
                                        <td>{ dynamicData. specialQualityRequirement}</td>
										<td>  {dynamicData.material} </td> 
										<td> {dynamicData.poQuantity} </td>
                                        <td>  {dynamicData.poDeliveryTerm} </td> 
                                        <td>    <DatePicker
                                        className="form-control"
                                        onChange={this.handleChange}
                                        selected={this.state.startDate}
                                        value= {dynamicData.poDeliveryDate}
                                        style = {{width : "auto"}} /> </td>
                                        <td>
                                         
                                        <DatePicker
                                        className="form-control"
                                        onChange={this.handleChange}
                                        selected={this.state.startDate}
                                        value=  {dynamicData.vendorCommitementDate}
                                        style = {{width : "auto"}} /> </td>
                                        <td> {dynamicData.vendorProductCode} </td>
										<td> 

                                        <DatePicker
                                        className="form-control"
                                        onChange={this.handleChange}
                                        selected={this.state.startDate}
                                        value=  {dynamicData.dModl2dDrwClrDate}
                                         /> </td>

										<td>
                                        <DatePicker
                                        className="form-control"
                                        onChange={this.handleChange}
                                        selected={this.state.startDate}
                                        value= {dynamicData.patternDate}
                                        /> </td>

										<td> 
                                        <DatePicker
                                        className="form-control"
                                        onChange={this.handleChange}
                                        selected={this.state.startDate}
                                        value=  {dynamicData.pouringDate} 
                                       /> </td> 
                                         
										<td> 
                                        <DatePicker
                                        className="form-control"
                                        onChange={this.handleChange}
                                        selected={this.state.startDate}
                                        value=  {dynamicData.castRMCDate}
                                        style = {{width : "auto"}} /> </td>
 

                                        
										<td>  
                                        <DatePicker
                                        className="form-control"
                                        onChange={this.handleChange}
                                        selected={this.state.startDate}
                                        value=  {dynamicData.machiningDate} 
                                        style = {{width : "auto"}} /> </td> 
                                         
                                        
                                        <td> 
                                        <DatePicker
                                        className="form-control"
                                        onChange={this.handleChange}
                                        selected={this.state.startDate}
                                        value= {dynamicData.hgsInspDate}
                                        style = {{width : "auto"}} /> </td>
										 
                                        <td>{dynamicData.packExWrkDispatch}</td>
                                        <td> 
                                        <DatePicker
                                        className="form-control"
                                        onChange={this.handleChange}
                                        selected={this.state.startDate}
                                        value=  {dynamicData.etdDate}
                                        style = {{width : "auto"}} />  </td> 
										 <td>
                                        <DatePicker
                                        className="form-control"
                                        onChange={this.handleChange}
                                        selected={this.state.startDate}
                                        value=  {dynamicData.koreaReachDate}
                                        style = {{width : "auto"}} />  </td>
                                       
										 
										<td>{dynamicData.status}</td>
										<td>  {dynamicData.dispatchQty} </td> 
										<td> {dynamicData.pendingQty} </td>
                                        <td>{dynamicData.vendorInvoiceNo}</td>
                                        <td> 
                                        <DatePicker
                                        className="form-control"
                                        onChange={this.handleChange}
                                        selected={this.state.startDate}
                                        value= {dynamicData.vendorInvoiceDate} 
                                        style = {{width : "auto"}} /> </td>
										<td>
                                        <DatePicker
                                        className="form-control"
                                        onChange={this.handleChange}
                                        selected={this.state.startDate}
                                        value=  {dynamicData.actualExworksDate}
                                        style = {{width : "auto"}} /> </td>
										<td>{dynamicData.delayinDays}</td>
										<td>  {dynamicData.delayCondition} </td> 
										<td> {dynamicData.delayReason} </td>

										<td><i class="fa fa-eye" aria-hidden="true" style = {{ color : '#00CCCC' }} onClick={this.addDetails}></i> </td>
									</tr>
							) }
                            </tbody>
							</Table>
						</div>
                    </Card>
                    </Col>
                </Row>
				
            </Aux>
             
        );
    }
    
   }  
 
  export default manProcess;

  
