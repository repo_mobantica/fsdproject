import React, { Component } from 'react';
import {Row, Col, Card, Table, Button} from 'react-bootstrap';
import 'font-awesome/css/font-awesome.min.css';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import Aux from "../../hoc/_Aux";
import {GetData} from '../../services/GetData';
import  { NavLink , Redirect} from 'react-router-dom'
 
   class pattern extends Component{

	constructor(props){
        super(props);
        this.state = {
            startDate: new Date(),
            show: false,
            data: [],
			referrer: ""

        };
        this.addDetails = this.addDetails.bind(this);
      
        
    }

    componentDidMount() {
        GetData('patternmanagement',this.state).then((result) => {
            let responseJson = result;
			this.setState({data:responseJson.data});
           });
    }
    
    addDetails = () => {
        this.setState({referrer: './patternDetails'});
          
        }
    //   }
    handleChange = date => {
        this.setState({
          startDate: date
        });
    };

    render() {
		const {referrer} = this.state;
        if (referrer) return <Redirect to={referrer} />;
        return (
            <Aux>
                <Row>
                    <Col>
                      <Card> 
                      <Row >
                            <Col>
							{/* <NavLink to="./patternDetails"> Dashboard </NavLink> */}
                            {/* <input type="submit" className="btn btn-primary pull-right" value="Add" onClick={this.addDetails}  /> */}
                            </Col>
                        
                        </Row>
						<div className="table-responsive">
							<Table className="tat"> 
							<tr>
								<th>Id </th>
								<th>Vendor Name</th> 
								<th>Commodity</th>
								<th>Order From</th>
								<th>Project Name</th>
								<th>Project Number</th>
								{/* <th>PO Status</th> */}
								<th>PO Number</th>
								<th>PO Serial No.</th>
                                <th>Item Type</th>
								<th>Purchase Order Date </th>
								<th>Pump Model</th>
								<th>Item Description</th>
								<th>Item Code</th>
								<th>Casting Drawing Number</th>
								<th>Casting Drawing Revison</th>
								<th>Machining Drawing Number</th>
								<th>Machining Drawing Revison</th>
								<th>Material</th>
								<th>PO Quantity</th>
                                <th>Pattern Type</th>
                                <th>Pattern Cost</th>
                                <th>PO Delivery Date</th>
                                <th>Vendor Product Code</th>
                                <th>3D Model Receipt Date</th>
                                <th>3D Model / 2D Drawing Clear Date</th>
                                <th>Vendor Pattern 3D model Submitted date</th>
                                <th>  Vendor Pattern 3D model Approved date</th>
                                <th>Pattern Date</th>
                                <th>Pattern Inspection Date</th>
                                <th>Pattern Inspection Remark</th>
                                <th>Pattern 3D Scanning Report</th>
                                <th>Pattern Storage Receipt</th>
                                <th>Pattern Storage Receipt Date</th>
                                <th>Pattern Inspection Report</th>
                                <th>Pattern Invoice Number </th>
                                <th> Pattern Invoice Date</th>
                                <th>Pattern Invoice Amount</th>
                                <th>Pattern Documents Sharing Date</th>
                                <th>Pattern Documents Review </th>
                                <th>Enquiry Weight (Kg)</th>
                                <th>Weight as per 2D drawing </th>
                                <th>Pattern 3D Model Weight (Kg)</th>
                                <th>Actual Weight (Kg)</th>
                                <th>Weight Difference %</th>
								<th>Action</th>
							</tr>
							{
								this.state.data.map((dynamicData) =>
									<tr className="trow"> 
										<td>  {dynamicData.id} </td> 
										<td> {dynamicData.vendorName} </td>
										<td>{dynamicData.commodity}</td>
										<td>  {dynamicData.orderFrom} </td> 
										<td> {dynamicData.projectName} </td>
										<td>{dynamicData.projectNumber}</td>
                                        <td> {dynamicData.poNumber} </td>
                                        <td>{dynamicData.poSerialNo}</td>
                                        <td>{dynamicData.itemType}</td>
								        <td>  <DatePicker
										className="form-control"
										onChange={this.handleChange}
                                        selected={this.state.startDate}
										value={dynamicData.poDate} /> </td> 
										<td> {dynamicData.pumpModel} </td>
										<td>{dynamicData.itemDescription}</td>
										<td>  {dynamicData.itemCode} </td> 
										<td> {dynamicData.castingDrawingNumber} </td>
										<td>{dynamicData.castingDrawingRevison}</td>
										<td>  {dynamicData.machiningDrawingNumber} </td> 
										<td> {dynamicData.machiningDrawingRevison} </td>
										<td>  {dynamicData.material} </td> 
										<td> {dynamicData.poQuantity} </td>
                                        <td>  {dynamicData.patternType} </td> 
                                        <td> {dynamicData.patternCost} </td>
                                        <td><DatePicker
										className="form-control"
										onChange={this.handleChange}
                                        selected={this.state.startDate}
										value={dynamicData.poDeliveryDate} /></td>
                                        <td> {dynamicData.vendorProductCode} </td>
										<td><DatePicker
										className="form-control"
										onChange={this.handleChange}
                                        selected={this.state.startDate}
										value={dynamicData.dModelReceiptdate} /></td>
										<td><DatePicker
										className="form-control"
										onChange={this.handleChange}
                                        selected={this.state.startDate}
										value={dynamicData.dMod2dDrwClrDate} /></td>
										<td>  <DatePicker
										className="form-control"
										onChange={this.handleChange}
                                        selected={this.state.startDate}
										value={dynamicData.VenPatt3dModSubDate} /></td> 
                                        <td>  <DatePicker
										className="form-control"
										onChange={this.handleChange}
                                        selected={this.state.startDate}
										value={dynamicData.venPat3dmoApDate} /></td> 
                                        <td> <DatePicker
										className="form-control"
										onChange={this.handleChange}
                                        selected={this.state.startDate}
										value={dynamicData.patternDate} /></td>
                                        <td><DatePicker
										className="form-control"
										onChange={this.handleChange}
                                        selected={this.state.startDate}
										value={dynamicData.patternInspectionDate} /></td>
                                        <td>  {dynamicData.patternInspectionRemark} </td>
										<td> {dynamicData.pattern3dScanningReport} </td>
										<td> {dynamicData.patternStorageReceipt} </td>
										<td> <DatePicker
										className="form-control"
										onChange={this.handleChange}
                                        selected={this.state.startDate}
										value={dynamicData.patternStorageReceiptDate} /></td> 
                                        <td> {dynamicData.patternInspectionReport} </td>
                                        <td>{dynamicData.patternInvoiceNumber}</td>
                                        <td> <DatePicker
										className="form-control"
										onChange={this.handleChange}
                                        selected={this.state.startDate}
										value={dynamicData.patternInvoiceDate} /></td>
										<td> {dynamicData.patternInvoiceAmount}</td>
										<td><DatePicker
										className="form-control"
										onChange={this.handleChange}
                                        selected={this.state.startDate}
										value={dynamicData.patternDocSharingDate} /></td>
										<td>  {dynamicData.pDRCDKPTeam} </td> 
										<td>  {dynamicData.enquiryWeight}</td>
                                        <td> {dynamicData.weightasper2dDrawing}  </td>
                                        <td>{dynamicData.pattern3dModelWeight}</td>
                                        <td> {dynamicData.actualWeight} </td>
										<td>{dynamicData.weightDifference}</td>

										<td><i class="fa fa-eye" aria-hidden="true" style = {{ color : '#00CCCC' }} onClick={this.addDetails} ></i> </td>
									</tr>
							) }
							</Table>
						</div>
                    </Card>
                    </Col>
                </Row>
				
            </Aux>
             
        );
    }
    
   }  
 
  export default pattern;

  
