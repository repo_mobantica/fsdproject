import React from 'react';
import {Row, Col, Card, Form, Button, InputGroup, FormControl, DropdownButton, Dropdown} from 'react-bootstrap';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import Aux from "../../hoc/_Aux";
import {GetData} from '../../services/GetData';

class FormsElements extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            commodity : "",
            orderFrom : "",
            itemType : "",
            patternType : "",
            poDeliveryTerm : "",
            paymentTerm : "",
            castingDraweringRevision : "",
            machiningDraweringRevision : "",
            remark : "",
            patternCost : "",
            shippingmodeinvtypeArray:[],
            selectedOption:"",
            paymentTermArray:[],
            startDate: new Date()

        };
        this.onChange = this.onChange.bind(this);
        
    }
    componentDidMount() {
        GetData('shippingmodeinvtype',this.state).then((result) => {
            let responseJson = result;
            this.setState({shippingmodeinvtypeArray:responseJson.data});
           });
        }
    // state = {
    //     startDate: new Date()
    // };

    // handleChange = date => {
    //     this.setState({
    //       startDate: date
    //     });
    // };

    onChange = (e) => {
        this.setState({[e.target.name]:e.target.value});
    }

    render() {
 
        return (
            <Aux>
                <Row>
                    <Col>
                        <Card>
                            {/* <Card.Header>
                                <Card.Title as="h5">PO Management List</Card.Title>
                            </Card.Header> */}
                            <Card.Body>
                                {/* <h5>Form controls</h5> */}
                                <hr/>
                                <Row>
                                    <Col>
                                        <Form.Group controlId="formBasicEmail">
                                                <Form.Label>Vendor Name</Form.Label>
                                                <Form.Control type="text" placeholder="Enter vendor name" value="John" readOnly />
                                        </Form.Group>
                                    </Col>
                                    <Col>
                                        <Form.Group controlId="exampleForm.ControlInput1">
                                            <Form.Label>Order From</Form.Label>
                                            <Form.Control type="text" placeholder="Order From" value="12" readOnly/>
                                        </Form.Group>
                                    </Col>
                                    <Col>
                                        <Form.Group controlId="formBasicEmail">
                                            <Form.Label>Project Name</Form.Label>
                                            <Form.Control type="text" placeholder="Enter project name" value="Test" readOnly/>
                                        </Form.Group>
                                    </Col>
                                    <Col>
                                        <Form.Group controlId="formBasicEmail">
                                            <Form.Label>Project Number</Form.Label>
                                            <Form.Control type="text" placeholder="Enter project Number" value="142" readOnly/>
                                        </Form.Group>
                                    </Col>

                                    </Row>
                                    <Row>
                                        <Col>
                                            <Form.Group controlId="formBasicEmail">
                                                <Form.Label>PO Number</Form.Label>
                                                <Form.Control type="text" placeholder="Enter Po number" value="1452" readOnly/>
                                            </Form.Group>
                                        </Col>
                                        <Col>
                                            <Form.Group controlId="exampleForm.ControlInput1">
                                                <Form.Label>PO Serial NO</Form.Label>
                                                <Form.Control type="text" placeholder="PO serial no" value="147" readOnly/>
                                            </Form.Group>
                                        </Col>
                                        <Col>
                                            <Form.Group controlId="exampleForm.ControlInput1">
                                                <Form.Label>Pump Model</Form.Label>
                                                <Form.Control type="text" placeholder="Pump model" value="1245" readOnly/>
                                            </Form.Group>
                                        </Col>
                                        <Col>
                                            <Form.Group controlId="exampleForm.ControlInput1">
                                                <Form.Label>Item Description</Form.Label>
                                                <Form.Control type="textarea" placeholder="Item Description" value="Test" readOnly/>
                                            </Form.Group>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col>
                                        <Form.Group controlId="exampleForm.ControlInput1">
                                            <Form.Label>Item Code</Form.Label>
                                            <Form.Control type="text" placeholder="Item Code" value="145" readOnly/>
                                        </Form.Group>
                                        </Col>
                                        <Col>
                                        <Form.Group controlId="exampleForm.ControlInput1">
                                            <Form.Label>Material</Form.Label>
                                            <Form.Control type="number" placeholder="Material" value="147" readOnly/>
                                        </Form.Group>
                                        </Col>
                                        <Col>
                                        <Form.Group controlId="exampleForm.ControlInput1">
                                            <Form.Label>Pattern Invoice Number</Form.Label>
                                            <Form.Control type="text" placeholder="Pattern Invoice Number" value="145" readOnly/>
                                        </Form.Group>
                                        </Col>
                                        <Col>
                                        <Form.Group controlId="exampleForm.ControlInput1">
                                            <Form.Label>Pattern Invoice Date</Form.Label>
                                            <DatePicker className="form-control"
                                            value={"14/12/2018"}
                                            readOnly
                                            />
                                        </Form.Group>
                                        
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col>
                                        <Form.Group controlId="formBasicEmail">
                                            <Form.Label>Pattern Cost</Form.Label>
                                            <Form.Control type="text" placeholder="Enter Pattern Cost" value="1425" readOnly/>
                                        </Form.Group>
                                        </Col>
                                        <Col>
                                            <Form.Group controlId="exampleForm.ControlInput1">
                                                <Form.Label>BL / AWB Number</Form.Label>
                                                <Form.Control type="text" placeholder="BL / AWB Number" />
                                            </Form.Group>
                                        </Col>
                                        <Col>
                                            <Form.Group controlId="exampleForm.ControlInput1">
                                                <Form.Label>BL / AWB Date</Form.Label>
                                                <DatePicker className="form-control"
                                            value={"14/12/2018"}
                                            readOnly
                                            />
                                            </Form.Group>
                                        
                                        </Col>
                                        <Col>
                                        <Form.Group controlId="formBasicEmail">
                                                <Form.Label>Vendor Invoice No</Form.Label>
                                                <Form.Control type="text" placeholder="Enter vendor invoice No" value="John" readOnly />
                                        </Form.Group>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col>
                                        <Form.Group controlId="exampleForm.ControlInput1">
                                            <Form.Label>Vendor Invoice Date</Form.Label>
                                            <DatePicker className="form-control"
                                            value={"14/12/2018"}
                                            readOnly
                                            />
                                        </Form.Group>
                                        
                                        </Col>
                                        <Col>
                                        <Form.Group controlId="formBasicEmail">
                                                <Form.Label>Dispatch Quantity</Form.Label>
                                                <Form.Control type="text" placeholder="Enter Dispatch Quantity" value="145" readOnly />
                                        </Form.Group>
                                        </Col>
                                        <Col>
                                        <Form.Group controlId="formBasicEmail">
                                                <Form.Label>Unit Price</Form.Label>
                                                <Form.Control type="text" placeholder="Enter Unit Price" value="147" readOnly />
                                        </Form.Group>
                                        </Col>
                                        <Col>
                                        <Form.Group controlId="exampleForm.ControlInput1">
                                            <Form.Label> Invoice Unit Price</Form.Label>
                                            <Form.Control type="text" placeholder=" Invoice Unit Price" />
                                        </Form.Group>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col>
                                        <Form.Group controlId="formBasicEmail">
                                            <Form.Label>Total Invoice Amount</Form.Label>
                                            <Form.Control type="text" placeholder="Total Invoice Amount" />
                                        </Form.Group>
                                        </Col>
                                        <Col>
                                        <Form.Group controlId="formBasicEmail">
                                            <Form.Label>HGS Paid Amount</Form.Label>
                                            <Form.Control type="text" placeholder="Enter Order from" />
                                        </Form.Group>
                                        </Col>
                                        <Col>
                                        <Form.Group controlId="exampleForm.ControlInput1">
                                            <Form.Label>Pending Payment Amount </Form.Label>
                                            <Form.Control type="text" placeholder="Pending Payment Amount " />
                                        </Form.Group>
                                        </Col>
                                        <Col>
                                        <Form.Label>Shipping Mode / Invoice Type</Form.Label>
                                        <Form.Control size="small" as="select" className="mb-3">
                                            <option>Please select</option>
                                            {this.state.shippingmodeinvtypeArray.map((e, key) => {
                                                return <option key={key} value={e.id}>{e.value}</option>;
                                             })}
                                        </Form.Control>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col>
                                        <Form.Group controlId="exampleForm.ControlInput1">
                                            <Form.Label>Payment Due Date </Form.Label>
                                            <DatePicker className="form-control"
                                            value={"14/12/2018"}
                                            readOnly
                                            />
                                        </Form.Group>
                                        </Col>
                                        <Col>
                                            <Form.Group controlId="exampleForm.ControlInput1">
                                                <Form.Label>Payment Due From (No. of Days)</Form.Label>
                                                <Form.Control type="text" placeholder="Payment Due From (No. of Days)"  />
                                            </Form.Group>
                                        </Col>
                                        <Col>
                                        <Form.Group controlId="exampleForm.ControlSelect1">
                                            <Form.Label>HGS Promised Payment Date </Form.Label>
                                            <DatePicker className="form-control"
                                            value={"14/12/2018"}
                                            readOnly
                                            />
                                            </Form.Group>
                                            
                                        </Col>
                                        <Col>
                                        <Form.Group controlId="formBasicEmail">
                                                <Form.Label>Payment Result</Form.Label>
                                                <Form.Control type="text" placeholder="Enter Payment Result" value="No" readOnly />
                                        </Form.Group>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col>
                                        <Form.Group controlId="formBasicEmail">
                                            <Form.Label>HGS Korea Purchasing Remark</Form.Label>
                                            <Form.Control type="text" placeholder="Enter HGS Korea Purchasing Remark" />
                                        </Form.Group>
                                        </Col>
                                        <Col>
                                        <Form.Group controlId="exampleForm.ControlInput1">
                                            <Form.Label>HGS India Purchasing Remark</Form.Label>
                                            <Form.Control type="text" placeholder="HGS India Purchasing Remark" />
                                        </Form.Group>
                                        </Col>
                                        <Col>
                                        <Form.Group controlId="formBasicEmail">
                                            <Form.Label>Vendor Remark</Form.Label>
                                            <Form.Control type="text" placeholder="Vendor Remark" />
                                        </Form.Group>
                                        </Col>
                                        <Col>
                                        </Col>
                                    </Row>
                                <Row>
                                    <Button variant="primary">
                                        Submit
                                    </Button>
                                </Row>
                            </Card.Body>
                            </Card>
                    </Col>
                </Row>
            </Aux>
        );
    }
}

export default FormsElements;
