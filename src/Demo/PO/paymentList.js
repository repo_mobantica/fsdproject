import React, { Component } from 'react';
import {Row, Col, Card, Table, Button} from 'react-bootstrap';
import 'font-awesome/css/font-awesome.min.css';
import Aux from "../../hoc/_Aux";
import {GetData} from '../../services/GetData';
import  { NavLink , Redirect} from 'react-router-dom'
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
 
   class payment extends Component{

	constructor(props){
        super(props);
        this.state = {
            show: false,
            data: [],
			referrer: ""

        };
        this.addDetails = this.addDetails.bind(this);
      
        
    }

    componentDidMount() {
        GetData('payment',this.state).then((result) => {
            let responseJson = result;
			this.setState({data:responseJson.data});
           });
    }
    
    addDetails = () => {
        this.setState({referrer: './paymentDetails'});
          
        }
    //   }
 

    render() {
		const {referrer} = this.state;
        if (referrer) return <Redirect to={referrer} />;
        return (
            <Aux>
                <Row>
                    <Col>
                      <Card> 
                      <Row >
                            <Col>
							{/* <NavLink to="./logisticDetails"> Dashboard </NavLink> */}
                            {/* <input type="submit" className="btn btn-primary pull-right" value="Add" onClick={this.addDetails}  /> */}
                            </Col>
                        
                        </Row>
						<div className="table-responsive">
							<Table className="tat"> 
							<tr>
								<th>Id </th>
                                <th>Vendor Name</th> 
                                <th>Order From</th>
                                <th>Project Name</th>
								<th>Project Number</th>
                                <th>PO Number</th>
                                <th>PO Serial No.</th> 
                                <th>Pump Model</th>
								<th>Item Description</th>
								<th>Item Code</th>
                                <th>Material</th>
                                <th>Pattern Invoice Number</th>
                                <th>Pattern Invoice Date</th>
					       	<th>Pattern Cost </th>
                                <th>BL / AWB Number</th>
                                <th>BL / AWB Date</th>
                                <th>Vendor Invoice No.</th>
                                <th>Vendor Invoice Date</th>
                                <th>Dispatch Qty</th>
								<th>Unit Price </th>
								<th>Invoice Unit Price $</th>
								<th>Total Invoice Amount $</th>
								<th>HGS Paid Amount $</th>
                                <th>Pending Payment Amount </th>
                                <th>Shipping Mode / Invoice Type</th>
                                <th>Payment Due Date</th>
								<th>Payment Due From (No. of Days)</th>
								<th>HGS Promised Payment Date</th>
								<th>Payment Result</th>
								<th>HGS Korea Purchasing Remark</th>
								<th>HGS India Purchasing Remark</th>
								<th>Vendor Remark</th>
								 
								<th>Action</th>
							</tr>
							{
								this.state.data.map((dynamicData) =>
									<tr className="trow"> 
										<td>  {dynamicData.id} </td> 
                                        <td> {dynamicData.vendorName} </td>
                                        <td>  {dynamicData.orderFrom} </td> 
										<td> {dynamicData.projectName} </td>
										<td>{dynamicData.projectNumber}</td>
                                        <td> {dynamicData.poNumber} </td>
										<td>{dynamicData.poSerialNo}</td>
                                        <td> {dynamicData.pumpModel} </td>
										<td>{dynamicData.itemDescription}</td>
										<td>  {dynamicData.itemCode} </td> 
                                        <td> { dynamicData.material} </td>
										<td>{dynamicData.patternInvoiceNo}</td>
										
										<td> <DatePicker
                                        className="form-control"
                                        onChange={this.handleChange}
                                        selected={this.state.startDate}
                                        value=  {dynamicData.patternInvoiceDate}
                                        style = {{width : "auto"}} /> </td> 
										
										<td> {dynamicData.patternCost} </td>
										<td>{dynamicData.blAwbNo}</td>
										<td>  <DatePicker
                                        className="form-control"
                                        onChange={this.handleChange}
                                        selected={this.state.startDate}
                                        value=  {dynamicData.blAwbDate}
                                        style = {{width : "auto"}} /> </td> 
										<td> {dynamicData.vendorInvoiceNo} </td>
										<td> <DatePicker
                                        className="form-control"
                                        onChange={this.handleChange}
                                        selected={this.state.startDate}
                                        value=   {dynamicData.vendorInvoiceDate}
                                        style = {{width : "auto"}} /> </td> 
										<td> {dynamicData.dispatchQty} </td>

										<td>{dynamicData.unitPrice}</td>
										<td>  {dynamicData.invoiceUnitPrice} </td> 
										<td> {dynamicData.totalInvoiceAmt} </td>
										<td>{dynamicData.hgsPaidAmt}</td>
										<td>{dynamicData.pendingPaymentAmt}</td>
										<td>  {dynamicData.ShippingModeInvoiceType} </td> 
										<td> <DatePicker
                                        className="form-control"
                                        onChange={this.handleChange}
                                        selected={this.state.startDate}
                                        value=  {dynamicData.paymentDueDate}
                                        style = {{width : "auto"}} />  </td>

                                        <td>{dynamicData.paymentDueFrom}</td>
										<td><DatePicker
                                        className="form-control"
                                        onChange={this.handleChange}
                                        selected={this.state.startDate}
                                        value=  {dynamicData.hgsPromisedPaymentDate}
                                        style = {{width : "auto"}} /> </td>
										<td>  {dynamicData.paymentResult} </td> 
										<td> {dynamicData.hgsKoreaPurchasingRemark} </td>
                                        <td>{dynamicData.hgsIndiaPurchasingRemark}</td>
                                        <td>{dynamicData.vendorRemark}</td>
										 
										<td><i class="fa fa-eye" aria-hidden="true" style = {{ color : '#00CCCC' }} onClick={this.addDetails}></i> </td>
									</tr>
							) }
							</Table>
						</div>
                    </Card>
                    </Col>
                </Row>
				
            </Aux>
             
        );
    }
    
   }  
 
  export default payment;

  
