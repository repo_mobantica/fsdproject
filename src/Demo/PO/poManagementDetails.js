import React from 'react';
import {Row, Col, Card, Form} from 'react-bootstrap';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import Aux from "../../hoc/_Aux";
import {PostData} from '../../services/PostData';
import {GetData} from '../../services/GetData';

class FormsElements extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            commodity : "",
            orderFrom : "",
            itemType : "",
            patternType : "",
            poDeliveryTerm : "",
            paymentTerm : "",
            castingDraweringRevision : "",
            machiningDraweringRevision : "",
            remark : "",
            patternCost : "",
            commodityArray:[],
            selectedOption:"",
            orderfromArray:[],
            itemtypeArray:[],
            patterntypeArray:[],
            podeliverytermArray:[],
            paymentTermArray:[],
            startDate: new Date()

        };
        this.addPoDetails = this.addPoDetails.bind(this);
        this.onChange = this.onChange.bind(this);
        
    }
    componentDidMount() {
        GetData('commodity',this.state).then((result) => {
            let responseJson = result;
            this.setState({commodityArray:responseJson.data});
           });
           GetData('orderfrom',this.state).then((result) => {
            let responseJson = result;
            this.setState({orderfromArray:responseJson.data});
           });
           GetData('itemtype',this.state).then((result) => {
            let responseJson = result;
            this.setState({itemtypeArray:responseJson.data});
           });
           GetData('patterntype',this.state).then((result) => {
            let responseJson = result;
            this.setState({patterntypeArray:responseJson.data});
           });
           GetData('podeliveryterm',this.state).then((result) => {
            let responseJson = result;
            this.setState({podeliverytermArray:responseJson.data});
           });
           GetData('podeliveryterm',this.state).then((result) => {
            let responseJson = result;
            this.setState({poDeliverytermArray:responseJson.data});
           });
    }

    addPoDetails () {
        PostData('addpodetails',this.state).then((result) => {  
            //let responseJson = result;
          //   if(responseJson.userData){         
          //     sessionStorage.setItem('userData',JSON.stringify(responseJson));
          //     this.setState({redirectToReferrer: true});
          //   }
            
           });

    }
    handleChange(selectedOption) {
        this.setState({selectedOption});
       }
    onChange = (e) => {
        this.setState({[e.target.name]:e.target.value});
    }

    render() {
        return (
            <Aux>
                <Row>
                    <Col>
                        <Card>
                            {/* <Card.Header>
                                <Card.Title as="h5">PO Management List</Card.Title>
                            </Card.Header> */}
                            <Card.Body>
                                {/* <h5>Form controls</h5> */}
                                <hr/>
                                <Row>
                                    <Col md={3}>
                                        <Form>
                                            <Form.Group controlId="formBasicEmail">
                                                <Form.Label>Vendor Name</Form.Label>
                                                <Form.Control type="text" placeholder="Enter vendor name" value="John" readOnly />
                                            </Form.Group>
                                        </Form>
                                    </Col>
                                    <Col md={3}>
                                        <Form>
                                        <Form.Label>Commodity</Form.Label>
                                       
                                        <Form.Control size="small" as="select" className="mb-3">
                                            <option>Please select</option>
                                            {this.state.commodityArray.map((e, key) => {
                                                return <option key={key} value={e.id}>{e.value}</option>;
                                             })}
                                           
                                        </Form.Control>
                                        </Form>
                                    </Col>
                                    <Col md={3}>
                                        <Form>
                                            <Form.Group controlId="formBasicEmail">
                                                <Form.Label>Order From</Form.Label>
                                                <Form.Control size="small" as="select" className="mb-3">
                                                    <option>Please select</option>
                                                    {this.state.orderfromArray.map((e, key) => {
                                                        return <option key={key} value={e.id}>{e.value}</option>;
                                                    })}
                                                
                                                </Form.Control>
                                            </Form.Group>
                                        </Form>
                                    </Col>
                                    <Col md={3}>
                                        <Form>
                                            <Form.Group controlId="formBasicEmail">
                                                <Form.Label>Project Name</Form.Label>
                                                <Form.Control type="text" placeholder="Enter project name" value="Test" readOnly/>
                                            </Form.Group>
                                        </Form>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col md={3}>
                                        <Form>
                                            <Form.Group controlId="formBasicEmail">
                                                <Form.Label>Project Number</Form.Label>
                                                <Form.Control type="text" placeholder="Enter project number" value="1452" readOnly/>
                                            </Form.Group>
                                        </Form>
                                    </Col>
                                    <Col md={3}>
                                        <Form>
                                            <Form.Group controlId="exampleForm.ControlInput1">
                                                <Form.Label>PO Status</Form.Label>
                                                <Form.Control type="text" placeholder="PO status" value="approved" readOnly/>
                                            </Form.Group>
                                        </Form>
                                    </Col>
                                    <Col md={3}>
                                        <Form>
                                            <Form.Group controlId="formBasicEmail">
                                                <Form.Label>PO Number</Form.Label>
                                                <Form.Control type="text" placeholder="Enter Po number" value="1452" readOnly/>
                                            </Form.Group>
                                        </Form>
                                    </Col>
                                    <Col md={3}>
                                        <Form>
                                            <Form.Group controlId="exampleForm.ControlInput1">
                                                <Form.Label>PO Serial NO</Form.Label>
                                                <Form.Control type="text" placeholder="PO serial no" value="147" readOnly/>
                                            </Form.Group>
                                        </Form>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col md={3}>
                                        <Form>
                                            <Form.Label>Item Type</Form.Label>
                                            <Form.Control size="small" as="select" className="mb-3">
                                            <option>Please select</option>
                                            {this.state.itemtypeArray.map((e, key) => {
                                                return <option key={key} value={e.id}>{e.value}</option>;
                                             })}
                                           
                                        </Form.Control>
                                        </Form>
                                    </Col>
                                    <Col md={3}>
                                        <Form>
                                        <Form.Group controlId="exampleForm.ControlSelect1">
                                        <Form.Label>PO Date</Form.Label>
                                        <DatePicker
                                        className="form-control"
                                        value={"14/12/2018"}
                                        readOnly
                                        />
                                        </Form.Group>
                                        
                                </Form>
                                    </Col>
                                    <Col md={3}>
                                        <Form>
                                            <Form.Group controlId="exampleForm.ControlInput1">
                                                <Form.Label>Pump Model</Form.Label>
                                                <Form.Control type="text" placeholder="Pump model" value="1245" readOnly/>
                                            </Form.Group>
                                        </Form>
                                    </Col>
                                    <Col md={3}>
                                        <Form>
                                            <Form.Group controlId="exampleForm.ControlInput1">
                                                <Form.Label>Item Description</Form.Label>
                                                <Form.Control type="textarea" placeholder="Item Description" value="Test" readOnly/>
                                            </Form.Group>
                                        </Form>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col md={3}>
                                        <Form>
                                            <Form.Group controlId="exampleForm.ControlInput1">
                                                <Form.Label>Item Code</Form.Label>
                                                <Form.Control type="text" placeholder="Item Code" value="145" readOnly/>
                                            </Form.Group>
                                        </Form>
                                    </Col>
                                    <Col md={3}>
                                        <Form>
                                            <Form.Group controlId="exampleForm.ControlInput1">
                                                <Form.Label>Casting Drawing Number</Form.Label>
                                                <Form.Control type="number" placeholder="Casting Drawing Number" value="147" readOnly/>
                                            </Form.Group>
                                        </Form>
                                    </Col>
                                    <Col md={3}>
                                        <Form>
                                            <Form.Group controlId="exampleForm.ControlInput1">
                                                <Form.Label>Casting Drawing Revision</Form.Label>
                                                <Form.Control type="text" placeholder="Casting Drawing Revision"  />
                                            </Form.Group>
                                        </Form>
                                    </Col>
                                    <Col md={3}>
                                        <Form>
                                            <Form.Group controlId="exampleForm.ControlInput1">
                                                <Form.Label>Machining Drawing Number</Form.Label>
                                                <Form.Control type="number" placeholder="Machining Drawing Number" value="123" readOnly/>
                                            </Form.Group>
                                        </Form>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col md={3}>
                                        <Form>
                                            <Form.Group controlId="exampleForm.ControlInput1">
                                                <Form.Label>Machining Drawing Revision</Form.Label>
                                                <Form.Control type="text" placeholder="Machining Drawing Revision" />
                                            </Form.Group>
                                        </Form>
                                    </Col>
                                    <Col md={3}>
                                        <Form>
                                            <Form.Group controlId="exampleForm.ControlInput1">
                                                <Form.Label>Special Quality Requirement</Form.Label>
                                                <Form.Control type="number" placeholder="Matching Drawering Number" value="147" readOnly/>
                                            </Form.Group>                
                                        </Form>
                                    </Col>
                                    <Col md={3}>
                                        <Form>
                                            <Form.Group controlId="exampleForm.ControlInput1">
                                                <Form.Label>Material</Form.Label>
                                                <Form.Control type="number" placeholder="Material" value="147" readOnly/>
                                            </Form.Group>
                                        </Form>
                                    </Col>
                                    <Col md={3}>
                                        <Form>
                                            <Form.Group controlId="exampleForm.ControlInput1">
                                                <Form.Label>PO Quantity</Form.Label>
                                                <Form.Control type="number" placeholder="PO Quantity" value="140" readOnly/>
                                            </Form.Group>
                                        </Form>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col md={3}>
                                        <Form>
                                            <Form.Group controlId="exampleForm.ControlInput1">
                                                <Form.Label>Unit Price</Form.Label>
                                                <Form.Control type="number" placeholder="Unit Price" value="4578" readOnly/>
                                            </Form.Group>
                                        </Form>
                                    </Col>
                                    <Col md={3}>
                                        <Form>
                                            <Form.Group controlId="exampleForm.ControlInput1">
                                                <Form.Label>Total Cost</Form.Label>
                                                <Form.Control type="number" placeholder="Total Cost" value="235" readOnly/>
                                            </Form.Group>
                                        </Form>
                                    </Col>
                                    <Col md={3}>
                                        <Form>
                                            <Form.Label>Pattern Type</Form.Label>
                                            <Form.Control size="small" as="select" className="mb-3">
                                            <option>Please select</option>
                                            {this.state.patterntypeArray.map((e, key) => {
                                                return <option key={key} value={e.id}>{e.value}</option>;
                                             })}
                                           
                                        </Form.Control>
                                        </Form>
                                    </Col>
                                    <Col md={3}>
                                        <Form>
                                            <Form.Group controlId="formBasicEmail">
                                                <Form.Label>Pattern Cost</Form.Label>
                                                <Form.Control type="text" placeholder="Enter Pattern Cost" />
                                            </Form.Group>
                                        </Form>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col md={3}>
                                        <Form>
                                            <Form.Label>PO Delivery Term</Form.Label>
                                            <Form.Control size="small" as="select" className="mb-3">
                                            <option>Please select</option>
                                            {this.state.podeliverytermArray.map((e, key) => {
                                                return <option key={key} value={e.id}>{e.value}</option>;
                                             })}
                                           
                                        </Form.Control>
                                        </Form>
                                    </Col>
                                    <Col md={3}>
                                        <Form>
                                            <Form.Group controlId="exampleForm.ControlInput1">
                                                <Form.Label>PO Delivery (Korea Reaching Date)</Form.Label>
                                                <DatePicker className="form-control"
                                                onChange={this.onChange}
                                                value={"14/12/2018"}
                                            />
                                            </Form.Group>
                                           
                                        </Form>
                                    </Col>
                                    <Col md={3}>
                                        <Form>
                                            <Form.Label>Payment Term</Form.Label>
                                            <Form.Control size="small" as="select" className="mb-3">
                                            <option>Please select</option>
                                            {this.state.paymentTermArray.map((e, key) => {
                                                return <option key={key} value={e.id}>{e.value}</option>;
                                             })}
                                           
                                        </Form.Control>
                                        </Form>
                                    </Col>
                                    <Col md={3}>
                                        <Form>
                                            <Form.Group controlId="exampleForm.ControlInput1">
                                                <Form.Label>Remark</Form.Label>
                                                <Form.Control type="text" placeholder="Remark" />
                                            </Form.Group>
                                        </Form>
                                    </Col>
                                </Row>
                                
                                <Row>
                                <input type="submit" className="btn btn-primary" value="Save" onClick={this.addPoDetails}/>
                                </Row>
                            </Card.Body>
                        </Card>
                    </Col>
                </Row>
            </Aux>
        );
    }
}

export default FormsElements;
