import React, { Component } from 'react';
import {Row, Col, Card, Table, Button} from 'react-bootstrap';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import 'font-awesome/css/font-awesome.min.css';
import Aux from "../../hoc/_Aux";
import {GetData} from '../../services/GetData';
import  { NavLink , Redirect} from 'react-router-dom'
 
   class poManagement extends Component{

	constructor(props){
        super(props);
        this.state = {
			startDate: new Date(),
            show: false,
            data: [],
			referrer: ""

        };
        this.addDetails = this.addDetails.bind(this);
      
        
    }
	handleChange = date => {
        this.setState({
          startDate: date
        });
    };
    componentDidMount() {
        GetData('pomanagement',this.state).then((result) => {
            let responseJson = result;
			this.setState({data:responseJson.data});
           });
    }
    
    addDetails = () => {
        this.setState({referrer: './poManagementDetails'});
          
        }
    //   }
 

    render() {
		const {referrer} = this.state;
        if (referrer) return <Redirect to={referrer} />;
        return (
            <Aux>
                <Row>
                    <Col>
                      <Card> 
                      {/* <Row >
                            <Col>
						 
                            <input type="submit" className="btn btn-primary pull-right" value="Add" onClick={this.addDetails}  />
                            </Col>
                        
                        </Row> */}
						<div className="table-responsive">
							<Table className="tat"> 
							<tr>
								<th>Id </th>
								<th>Vendor Name</th> 
								<th>Commodity</th>
								<th>Order From</th>
								<th>Project Name</th>
								<th>Project Number</th>
								<th>PO Status</th>
								<th>PO Number</th>
								<th>PO Serial No.</th>
                                <th>Item Type</th>
								<th>Purchase Order Date </th>
								<th>Pump Model</th>
								<th>Item Description</th>
								<th>Item Code</th>
								<th>Casting Drawing Number</th>
								<th>Casting Drawing Revison</th>
								<th>Machining Drawing Number</th>
								<th>Machining Drawing Revison</th>
                                <th>Special Quality Requirement</th>
								<th>Material</th>
								<th>PO Quantity</th>
                                <th>Unit Price</th>
                                <th>Total Cost</th>
                                <th>Pattern Type</th>
                                <th>Pattern Cost</th>
                                <th>PO Delivery Term</th>
                                <th colSpan="2">PO Delivery Date</th>
                                <th>Payment Term</th>
								<th>Remark</th>
								<th>Action</th>
							</tr>
							{
								this.state.data.map((dynamicData) =>
									<tr className="trow"> 
										<td>  {dynamicData.id} </td> 
										<td> {dynamicData.vendorName} </td>
										<td>{dynamicData.commodity}</td>
										<td>  {dynamicData.orderFrom} </td> 
										<td> {dynamicData.projectName} </td>
										<td>{dynamicData.projectNumber}</td>
										<td>  {dynamicData.poStatus} </td> 
										<td> {dynamicData.poNumber} </td>
                                        <td>{dynamicData.itemType}</td>
										<td>{dynamicData.poSerialNo}</td>
										<td ><DatePicker
										className="form-control"
										onChange={this.handleChange}
                                        selected={this.state.startDate}
										value={dynamicData.poDate} />
										</td> 
										<td> {dynamicData.pumpModel} </td>
										<td>{dynamicData.itemDescription}</td>
										<td>  {dynamicData.itemCode} </td> 
										<td> {dynamicData.castingDrawingNo} </td>
										<td>{dynamicData.castingDrawingRevison}</td>
										<td>  {dynamicData.machiningDrawingNumber} </td> 
										<td> {dynamicData.machiningDrawingRevison} </td>
                                        <td> {dynamicData.specialQualityRequirement} </td>
										<td>  {dynamicData.material} </td> 
										<td> {dynamicData.poQuantity} </td>
                                        <td> {dynamicData.unitPrice} </td>
										<td>{dynamicData.totalCost}</td>
										<td>  {dynamicData.patternType} </td> 
										<td> {dynamicData.patternCost} </td>
										<td>{dynamicData.poDeliveryTerm}</td>
										<td colSpan="2"><DatePicker
										className="form-control"
										onChange={this.handleChange}
                                        selected={this.state.startDate}
										value={dynamicData.poDeliveryDate} /></td>
										<td>  {dynamicData.paymentTerm} </td> 
										<td> {dynamicData.Remark} </td>
										<td><i class="fa fa-eye" aria-hidden="true" style = {{ color : '#00CCCC' }} onClick={this.addDetails}></i> </td>
									</tr>
							) }
							</Table>
						</div>
                    </Card>
                    </Col>
                </Row>
				
            </Aux>
             
        );
    }
    
   }  
 
  export default poManagement;

  
