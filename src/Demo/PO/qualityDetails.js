import React from 'react';
import {Row, Col, Card, Form, Button} from 'react-bootstrap';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import Aux from "../../hoc/_Aux";
import {GetData} from '../../services/GetData';

class FormsElements extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            commodity : "",
            orderFrom : "",
            itemType : "",
            patternType : "",
            poDeliveryTerm : "",
            paymentTerm : "",
            castingDraweringRevision : "",
            machiningDraweringRevision : "",
            remark : "",
            patternCost : "",
            finalconcusionArray:[],
            selectedOption:"",
            paymentTermArray:[],
            startDate: new Date()

        };
        this.onChange = this.onChange.bind(this);
        
    }
    componentDidMount() {
        GetData('finalconcusion',this.state).then((result) => {
            let responseJson = result;
            this.setState({finalconcusionArray:responseJson.data});
           });
        }
    // state = {
    //     startDate: new Date()
    // };

    // handleChange = date => {
    //     this.setState({
    //       startDate: date
    //     });
    // };

    onChange = (e) => {
        this.setState({[e.target.name]:e.target.value});
    }

    onClickHandler = () => {
        const data = new FormData() 
        data.append('file', this.state.selectedFile)
    }

    render() {
 
        return (
            <Aux>
                <Row>
                    <Col>
                        <Card>
                            {/* <Card.Header>
                                <Card.Title as="h5">PO Management List</Card.Title>
                            </Card.Header> */}
                            <Card.Body>
                                {/* <h5>Form controls</h5> */}
                                <hr/>
                                <Row>
                                    <Col>
                                        <Form.Group controlId="formBasicEmail">
                                            <Form.Label>PO Number</Form.Label>
                                            <Form.Control type="text" placeholder="Enter Po number" value="1452" readOnly/>
                                        </Form.Group>
                                    </Col>
                                    <Col>
                                        <Form.Group controlId="exampleForm.ControlInput1">
                                            <Form.Label>PO Serial NO</Form.Label>
                                            <Form.Control type="text" placeholder="PO serial no" />
                                        </Form.Group>
                                    </Col>
                                    <Col>
                                    <Form.Group controlId="formBasicEmail">
                                                <Form.Label>Vendor Name</Form.Label>
                                                <Form.Control type="text" placeholder="Enter vendor name" value="John" readOnly />
                                        </Form.Group>
                                    </Col>
                                    <Col>
                                        <Form.Group controlId="formBasicEmail">
                                                <Form.Label>Commodity</Form.Label>
                                                <Form.Control type="text" placeholder="Enter Commodity" value="Yes" readOnly />
                                        </Form.Group>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col>
                                    <Form.Group controlId="exampleForm.ControlInput1">
                                            <Form.Label>Order From</Form.Label>
                                            <Form.Control type="text" placeholder="Order From" value="12" readOnly/>
                                        </Form.Group>
                                    </Col>
                                    <Col>
                                    <Form.Group controlId="exampleForm.ControlInput1">
                                            <Form.Label>Project Name</Form.Label>
                                            <Form.Control type="text" placeholder="Project Name" value="test" readOnly/>
                                        </Form.Group>
                                    </Col>
                                    <Col>
                                    <Form.Group controlId="exampleForm.ControlInput1">
                                            <Form.Label>Project Number</Form.Label>
                                            <Form.Control type="text" placeholder="Project Number" value="12" readOnly/>
                                        </Form.Group>
                                    </Col>
                                    <Col>
                                    <Form.Group controlId="formBasicEmail">
                                            <Form.Label>Po Date</Form.Label>
                                            <DatePicker
                                        className="form-control"
                                        value={"14/12/2018"}
                                        readOnly
                                        />
                                        </Form.Group>
                                       
                                    </Col>
                                </Row>
                                <Row>
                                    <Col>
                                        <Form.Group controlId="exampleForm.ControlInput1">
                                            <Form.Label>Pump Model</Form.Label>
                                            <Form.Control type="text" placeholder="Pump model" value="1245" readOnly/>
                                        </Form.Group>
                                    </Col>
                                    <Col>
                                    <Form.Group controlId="exampleForm.ControlInput1">
                                            <Form.Label>Item Description</Form.Label>
                                            <Form.Control type="textarea" placeholder="Item Description" value="Test" readOnly/>
                                        </Form.Group>
                                    </Col>
                                    <Col>
                                    <Form.Group controlId="exampleForm.ControlInput1">
                                            <Form.Label>Item Code</Form.Label>
                                            <Form.Control type="text" placeholder="Item Code" value="145" readOnly/>
                                        </Form.Group>
                                    </Col>
                                    <Col>
                                    <Form.Group controlId="exampleForm.ControlInput1">
                                                <Form.Label>Casting Drawering Number</Form.Label>
                                                <Form.Control type="number" placeholder="Casting Drawering Number" value="147" readOnly/>
                                            </Form.Group>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col>
                                        <Form.Group controlId="exampleForm.ControlInput1">
                                            <Form.Label>Casting Drawering Revision</Form.Label>
                                            <Form.Control type="text" placeholder="Casting Drawering Revision" value="147" readOnly/>
                                        </Form.Group>
                                    </Col>
                                    <Col>
                                        <Form.Group controlId="exampleForm.ControlInput1">
                                            <Form.Label>Machining Drawering Number</Form.Label>
                                            <Form.Control type="text" placeholder="Machining Drawering Number" value="147" readOnly/>
                                            </Form.Group>
                                    </Col>
                                    <Col>
                                    <Form.Group controlId="exampleForm.ControlInput1">
                                            <Form.Label>Machining Drawering Revision</Form.Label>
                                            <Form.Control type="text" placeholder="Machining Drawering Revision" value="Test" readOnly/>
                                            </Form.Group>
                                    </Col>
                                    <Col>
                                        <Form.Group controlId="exampleForm.ControlInput1">
                                            <Form.Label>Material</Form.Label>
                                            <Form.Control type="number" placeholder="Material" value="147" readOnly/>
                                        </Form.Group>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col>
                                        <Form.Group controlId="formBasicEmail">
                                            <Form.Label>NCR Number</Form.Label>
                                            <Form.Control type="text" placeholder="NCR Number" value="1452" readOnly/>
                                        </Form.Group>
                                    </Col>
                                    <Col>
                                        <Form.Group controlId="formBasicEmail">
                                            <Form.Label>NCR Quantity</Form.Label>
                                            <Form.Control type="text" placeholder="NCR Quantity" value="1452" readOnly/>
                                        </Form.Group>
                                    </Col>
                                    <Col>
                                    <Form.Group controlId="exampleForm.ControlInput1">
                                            <Form.Label>NCR Date</Form.Label>
                                            <DatePicker
                                        className="form-control"
                                        value={"14/12/2018"}
                                        readOnly
                                        />
                                        </Form.Group>
                                        
                                    </Col>
                                    <Col>
                                    <Form.Group controlId="exampleForm.ControlInput1">
                                            <Form.Label>NCR Brief deatils</Form.Label>
                                            <Form.Control type="text" placeholder="NCR Brief deatils" ></Form.Control>
                                        </Form.Group>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col>
                                    <Form.Label> HGS India Quality Team comment</Form.Label>
                                        <Form.Group style={{ display:'flex' , flexDirection:'row' }}>
                                        <input type="file" name="file" onChange={this.onChangeHandler}/>
                                        {/* <button type="button" class="btn btn-success btn-block" onClick={this.onClickHandler} style={{width: '50%' }}>Upload</button>  */}
                                        </Form.Group> 
                                    </Col>
                                    <Col>
                                    <Form.Label>HGS Korea Design Team comment </Form.Label>
                                        <Form.Group style={{ display:'flex' , flexDirection:'row' }}>
                                        <input type="file" name="file" onChange={this.onChangeHandler}/>
                                        {/* <button type="button" class="btn btn-success btn-block" onClick={this.onClickHandler} style={{width: '50%' }}>Upload</button>  */}
                                        </Form.Group>
                                    </Col>
                                    <Col>
                                    <Form.Label>   HGS Korea Quality Team comment</Form.Label>
                                        <Form.Group style={{ display:'flex' , flexDirection:'row' }}>
                                        <input type="file" name="file" onChange={this.onChangeHandler}/>
                                        {/* <button type="button" class="btn btn-success btn-block" onClick={this.onClickHandler} style={{width: '50%' }}>Upload</button>  */}
                                        </Form.Group> 
                                    </Col>
                                    <Col md={3}>
                                    <Form.Label> NCR Photos</Form.Label>
                                        <Form.Group style={{ display:'flex' , flexDirection:'row' }}>
                                        <input type="file" name="file" onChange={this.onChangeHandler}/>
                                        {/* <button type="button" class="btn btn-success btn-block" onClick={this.onClickHandler} style={{width: '50%' }}>Upload</button>  */}
                                        </Form.Group> 
                                    </Col>
                                </Row>
                                    <Row>
                                        <Col md={3}>
                                            <Form.Label>Final Conclusion</Form.Label>
                                            <Form.Control size="small" as="select" className="mb-3">
                                                <option>Please select</option>
                                                {this.state.finalconcusionArray.map((e, key) => {
                                                return <option key={key} value={e.id}>{e.value}</option>;
                                             })}
                                            </Form.Control>
                                        </Col>
                                    </Row>
                                     
                                <Row>
                                    <Button variant="primary">
                                        Submit
                                    </Button>
                                </Row>
                            </Card.Body>
                            </Card>
                    </Col>
                </Row>
            </Aux>
        );
    }
}

export default FormsElements;
