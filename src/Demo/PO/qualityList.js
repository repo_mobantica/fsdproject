import React, { Component } from 'react';
import {Row, Col, Card, Table, Button} from 'react-bootstrap';
import 'font-awesome/css/font-awesome.min.css';
import Aux from "../../hoc/_Aux";
import {GetData} from '../../services/GetData';
import  { NavLink , Redirect} from 'react-router-dom'
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
 
   class quality extends Component{

	constructor(props){
        super(props);
        this.state = {
            show: false,
            data: [],
			referrer: ""

        };
		this.addDetails = this.addDetails.bind(this);
		this.addDetails = this.addDetails.bind(this);
      
        
    }

    componentDidMount() {
        GetData('quality',this.state).then((result) => {
            let responseJson = result;
			this.setState({data:responseJson.data});
           });
    }
    
    addDetails = () => {
        this.setState({referrer: './qualityDetails'});
          
        }
    //   }
 

    render() {
		const {referrer} = this.state;
        if (referrer) return <Redirect to={referrer} />;
        return (
            <Aux>
                <Row>
                    <Col>
                      <Card> 
                      <Row >
                            <Col>
							{/* <NavLink to="./logisticDetails"> Dashboard </NavLink> */}
                            <input type="submit" className="btn btn-primary pull-right" value="Add" onClick={this.addDetails}  />
                            </Col>
                        
                        </Row>
						<div className="table-responsive">
							<Table className="tat"> 
							<tr>
								<th>Id </th>
                                <th>PO Number</th>
                                <th>PO Serial No.</th> 
                                <th>Vendor Name</th> 
								<th>Commodity</th>
                                <th>Order From</th>
                                <th>Project Name</th>
								<th>Project Number</th>
								<th >Purchase Order Date </th>
								<th>Pump Model</th>
								<th>Item Description</th>
								<th>Item Code</th>
								<th>Casting Drawing Number</th>
								<th>Casting Drawing Revison</th>
								<th>Machining Drawing Number</th>
								<th>Machining Drawing Revison</th>
								<th>Material</th>
								<th>NCR Number</th>
								<th>NCR Quantity</th>
								<th>NCR Date</th>
								<th>NCR Brief Details</th>
								<th>HGS India Quality Team comment</th>
								<th>HGS Korea Design Team comment</th>
								<th>HGS Korea Quality Team comment</th>
								<th>NCR Photos</th>
								<th>Final Conclusion</th>
								<th>Action</th>
							</tr>
							{
								this.state.data.map((dynamicData) =>
									<tr className="trow"> 
										<td>  {dynamicData.id} </td> 
                                        <td> {dynamicData.poNumber} </td>
										<td>{dynamicData.poSerialNo}</td>
										<td> {dynamicData.vendorName} </td>
										<td>{dynamicData.commodity}</td>
										<td>  {dynamicData.orderFrom} </td> 
										<td> {dynamicData.projectName} </td>
										<td>{dynamicData.projectNumber}</td>
										<td > <DatePicker
                                        className="form-control"
                                        onChange={this.handleChange}
                                        selected={this.state.startDate}
                                        value={dynamicData.poDate} 
                                        style = {{width : "auto"}} /> </td> 
										<td> {dynamicData.pumpModel} </td>
										<td>{dynamicData.itemDescription}</td>
										<td>  {dynamicData.itemCode} </td> 
										<td> {dynamicData.castingDrawingNo} </td>
										<td>{dynamicData.castingDrawingRevison}</td>
										<td>  {dynamicData.machiningDrawingNumber} </td> 
										<td> {dynamicData.machiningDrawingRevison} </td>
										<td>  {dynamicData.material} </td> 
										<td> {dynamicData.ncrNo} </td>
										<td>{dynamicData.ncrQuanity}</td>
										<td><DatePicker
                                        className="form-control"
                                        onChange={this.handleChange}
                                        selected={this.state.startDate}
                                        value=  {dynamicData.ncrDate}  /> </td> 
										<td>{ dynamicData.ncrBriefDetails}</td>
										<td> {dynamicData.hgsIndiaQualityCmt} </td>
										<td>{dynamicData.hgsKoreaDesignCmt}</td>
										<td>{dynamicData.hgsKoreaQualityCmt}</td>
										<td>  {dynamicData.ncrPhotos} </td> 
										<td> {dynamicData.finalConclusion} </td>
										 
										<td><i class="fa fa-edit" aria-hidden="true" style = {{ color : '#00CCCC' }} onClick={this.editDetails} ></i>
											<i class="fa fa-eye" aria-hidden="true" style = {{ color : '#00CCCC' , marginLeft : '10px'}} onClick={this.viewDetails} ></i> </td>
									</tr>
							) }
							</Table>
						</div>
                    </Card>
                    </Col>
                </Row>
				
            </Aux>
             
        );
    }
    
   }  
 
  export default quality;

  
