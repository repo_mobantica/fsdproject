import React from 'react';
import {Row, Col, Card, Form, Button, InputGroup, FormControl, DropdownButton, Dropdown} from 'react-bootstrap';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import Aux from "../../hoc/_Aux";

class FormsElements extends React.Component {

    // state = {
    //     startDate: new Date()
    // };

    // handleChange = date => {
    //     this.setState({
    //       startDate: date
    //     });
    // };

    onClickHandler = () => {
        const data = new FormData() 
        data.append('file', this.state.selectedFile)
    }

    render() {
 
        return (
            <Aux>
                <Row>
                    <Col>
                        <Card>
                            {/* <Card.Header>
                                <Card.Title as="h5">PO Management List</Card.Title>
                            </Card.Header> */}
                            <Card.Body>
                                {/* <h5>Form controls</h5> */}
                                <hr/>
                                <Row>
                                    <Col md={6}>
                                        <Form>
                                        <Form.Label>Order From</Form.Label>
                                        <Form.Control size="small" as="select" className="mb-3">
                                            <option>Please select</option>
                                            <option>Changwon</option>
                                            <option>Gimhe</option>
                                        </Form.Control>
                                        <Form.Label>RFQ Status</Form.Label>
                                        <Form.Control size="small" as="select" className="mb-3">
                                            <option>Please select</option>
                                            <option>Close</option>
                                            <option>Open</option>
                                        </Form.Control>
                                        <Form.Group controlId="exampleForm.ControlInput1">
                                            <Form.Label>RFQ Receipt from </Form.Label>
                                            <Form.Control type="text" placeholder="RFQ Receipt from" />
                                        </Form.Group>
                                        <Form.Group controlId="exampleForm.ControlInput1">
                                            <Form.Label>Project Number</Form.Label>
                                            <Form.Control type="text" placeholder="Project Number" />
                                        </Form.Group>
                                        <Form.Group controlId="exampleForm.ControlInput1">
                                            <Form.Label>Item Code</Form.Label>
                                            <Form.Control type="text" placeholder="Item Code" />
                                        </Form.Group>
                                        <Form.Group controlId="exampleForm.ControlInput1">
                                            <Form.Label>Quantity</Form.Label>
                                            <Form.Control type="text" placeholder="Quantity"  />
                                        </Form.Group>
                                        <Form.Group controlId="exampleForm.ControlInput1">
                                            <Form.Label>Projected Delivery Date</Form.Label>
                                            <Form.Control type="date" />
                                        </Form.Group>
                                        <Form.Group controlId="exampleForm.ControlInput1">
                                            <Form.Label>RFQ Requested Vendor</Form.Label>
                                            <Form.Control type="text" placeholder ="RFQ Requested Vendor"/>
                                        </Form.Group>
                                        <Form.Group controlId="exampleForm.ControlInput1">
                                            <Form.Label>Quotation Receipt Date</Form.Label>
                                            <Form.Control type="date" />
                                        </Form.Group>
                                        <Form.Group controlId="exampleForm.ControlInput1">
                                            <Form.Label>Lowest Unit Price</Form.Label>
                                            <Form.Control type="text" placeholder="Lowest Unit Price"/>
                                        </Form.Group>
                                        <Form.Group controlId="exampleForm.ControlInput1">
                                            <Form.Label>Comparsion Submitted Date</Form.Label>
                                            <Form.Control type="date" placeholder="Comparsion Submitted Date"/>
                                        </Form.Group>
                                        <Form.Label>Quotes</Form.Label>
                                        <Form.Group style={{ display:'flex' , flexDirection:'row' }}>
                                        <input type="file" name="file" onChange={this.onChangeHandler}/>
                                        <button type="button" class="btn btn-success btn-block" onClick={this.onClickHandler} style={{width: '50%' }}>Upload</button> 
                                        </Form.Group> 
                                       
                                        </Form>
                                    </Col>
                                    <Col md={6}>
                                    <Form.Label>Korea Feedback</Form.Label>
                                        <Form.Control size="small" as="select" className="mb-3">
                                            <option>Please select</option>
                                            <option>Shifted to Korea</option>
                                            <option> India price is highs</option>
                                            <option>RFQ Cancelled</option>
                                            <option>Confirmed LCC vendor</option>
                                            <option>Other</option>
                                        </Form.Control>
                                        <Form.Group controlId="exampleForm.ControlInput1">
                                            <Form.Label>RFQ Receipt Date</Form.Label>
                                            <Form.Control type="date" />
                                        </Form.Group>
                                        
                                        <Form.Group controlId="exampleForm.ControlInput1">
                                            <Form.Label>Project Name</Form.Label>
                                            <Form.Control type="text" placeholder="Project Name"  />
                                        </Form.Group>
                                        <Form.Group controlId="exampleForm.ControlInput1">
                                            <Form.Label>Part Name</Form.Label>
                                            <Form.Control type="text" placeholder="Part Name"  />
                                        </Form.Group>
                                        <Form.Group controlId="exampleForm.ControlInput1">
                                            <Form.Label> Drawering Number</Form.Label>
                                            <Form.Control type="number" placeholder=" Drawering Number"  />
                                        </Form.Group>
                                        <Form.Group controlId="exampleForm.ControlInput1">
                                            <Form.Label> Weight </Form.Label>
                                            <Form.Control type="number" placeholder="Weight"  />
                                        </Form.Group>
                                        <Form.Group controlId="exampleForm.ControlInput1">
                                            <Form.Label> Pattern Type </Form.Label>
                                            <Form.Control type="number" placeholder="Pattern Type"  />
                                        </Form.Group>
                                        <Form.Group controlId="exampleForm.ControlInput1">
                                            <Form.Label>Enquiry date with Vendor</Form.Label>
                                            <Form.Control type="date" />
                                        </Form.Group>
                                        <Form.Group controlId="exampleForm.ControlInput1">
                                            <Form.Label> Final Negotation Date</Form.Label>
                                            <Form.Control type="date" />
                                        </Form.Group>
                                        <Form.Group controlId="exampleForm.ControlInput1">
                                            <Form.Label>Pattern Cost</Form.Label>
                                            <Form.Control type="text" placeholder="Pattern Cost"/>
                                        </Form.Group>
                                        <Form.Group controlId="exampleForm.ControlInput1">
                                            <Form.Label>RFQ Serial</Form.Label>
                                            <Form.Control type="text" placeholder="RFQ Serial"/>
                                        </Form.Group>
                                     

                                    </Col>
                                </Row>
                                <Row>
                                    <Button variant="primary">
                                        Submit
                                    </Button>
                                </Row>
                            </Card.Body>
                            </Card>
                    </Col>
                </Row>
            </Aux>
        );
    }
}

export default FormsElements;
