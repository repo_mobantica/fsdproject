import React from 'react';
import {Row, Col, Card, Table, Button} from 'react-bootstrap';
import 'font-awesome/css/font-awesome.min.css';
import Aux from "../../hoc/_Aux";

class BootstrapTable extends React.Component {

    constructor(){
        super();
    }
    // routeChange=()=> {
    //     let path = `newPath`;
         
    //   }
   

    render() {
        return (
            <Aux>
                <Row>
                    <Col>
                        <Card>
                            <Card.Header>
                                {/* <Card.Title as="h5">Basic Table</Card.Title>
                                <span className="d-block m-t-5">use bootstrap <code>Table</code> component</span> */}
                            </Card.Header>
                            <Card.Body>
                                <Table responsive>
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>RFQ Status</th>
                                        <th>Project Number</th>
                                        <th>Drawing Number</th>
                                        <th>RFQ Receipt Date </th>
                                        <th>View/ Approve</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <th scope="row">1</th>
                                        <td>Active</td>
                                        <td>1452</td>
                                        <td>456</td>
                                        <td>14/07/2019</td>
                                        {/* <td><Button color="primary" className="px-4"
                onClick={this.routeChange}>
                  
                  Login
                </Button></td> */}
                                        <td ><i class="fa fa-eye" aria-hidden="true" style = {{ color : '#00CCCC' }}></i></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">2</th>
                                        <td>Inactive</td>
                                        <td>145</td>
                                        <td>145</td>
                                        <td>12/10/2018</td>
                                        <td ><i class="fa fa-eye" aria-hidden="true" style = {{ color : '#00CCCC' }}></i></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">3</th>
                                        <td>Active</td>
                                        <td>145</td>
                                        <td>147</td>
                                        <td>12/10/2018</td>
                                        <td ><i class="fa fa-eye" aria-hidden="true" style = {{ color : '#00CCCC' }}></i></td>
                                    </tr>
                                    </tbody>
                                </Table>
                            </Card.Body>
                        </Card>
                       
                    </Col>
                </Row>
            </Aux>
        );
    }
}

export default BootstrapTable;