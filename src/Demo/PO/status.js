import React from 'react';
import {Row, Col, Card, Form, Button, Table, Modal } from 'react-bootstrap';
import "react-datepicker/dist/react-datepicker.css";
import Aux from "../../hoc/_Aux";
//import {PostData} from '../../services/PostData';
import {GetData} from '../../services/GetData';

class Settings extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            show: false,
            data: [],
            editvalue : ""

        };
        this.adduser = this.adduser.bind(this);
        this.onChange = this.onChange.bind(this);
        this.showModal = this.showModal.bind(this);
        
    }

    componentDidMount() {
        GetData('status',this.state).then((result) => {
            let responseJson = result;
            this.setState({data:responseJson.data});
           });
    }

    showModal = (value) => { 
        this.setState({ show: true ,
            editvalue : value
       });
         
      };
    
      hideModal = () => {
        this.setState({ show: false });
      };
    
    adduser = () => {
        // if(this.state.username && this.state.password && this.state.email && this.state.name){
            // const data = JSON.stringify({
            //     firstName : this.state.firstName,
            //     lastName : this.state.lastName,
            //     emailId : this.state.emailId,
            //     roleType : this.state.roleType,
            //     status : this.state.status
            // })
        // PostData('adduser',this.state).then((result) => {  
        //   let responseJson = result;
        //   if(responseJson.userData){         
        //     sessionStorage.setItem('userData',JSON.stringify(responseJson));
        //     this.setState({redirectToReferrer: true});
        //   }

         
            fetch('http://127.0.0.1:8000/api/test/')
            .then(function(response){
                response.json().then(function(resp){
                   
            });        
        
        });
          
        }
    //   }

    onChange = (e) => {
        this.setState({[e.target.name]:e.target.value});
    }

    render() {
 console.log("ST", this.state)
  
        return (
            <Aux>
                <Row>
                    <Col>
                      <Card> 
                      <Row>
                            <Col md={6}>
                                <Form.Group controlId="exampleForm.ControlInput1">
                                    <Form.Control type="text" placeholder="Enter item type" name="itemtype" onChange={this.onChange} value={this.state.firstName}/>
                                </Form.Group>
                            
                            </Col>
                            <Col md={6}>
                            <input type="submit" className="btn btn-primary" value="Add" onClick={this.adduser}  />
                            </Col>
                        
                        </Row>
                        <Table className="tat"> 
                        <tr><th>Id </th><th>Name</th><th>Action</th></tr>
                        {
                            this.state.data.map((dynamicData) =>
                                <tr className="trow"> <td>  {dynamicData.id} 
                            </td> <td> {dynamicData.value} </td>
                            <td><i className="fa fa-edit" aria-hidden="true" style = {{ color : '#00CCCC' }} onClick={ () => this.showModal(dynamicData.value) }></i>
                                <i className="fa fa-trash" aria-hidden="true" style = {{ color : '#00CCCC' , marginLeft : '10px'}}></i> 
                            </td>
                        </tr>
                        ) }
                        </Table>
                        {this.showModal && 
                        <Modal show={this.state.show} onHide={this.hideModal}>
                        <Modal.Header closeButton>
                          <Modal.Title>Edit</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            <Form.Group controlId="exampleForm.ControlInput1">
                                <Form.Control type="text"  name="itemtype" onChange={this.onChange} value={this.state.editvalue}/>
                            </Form.Group>
                        </Modal.Body>
                        <Modal.Footer>
                          <Button variant="secondary" onClick={this.hideModal}>
                            Close
                          </Button>
                          <Button variant="primary" onClick={this.hideModal}>
                            Save 
                          </Button>
                        </Modal.Footer>
                      </Modal>}
                    </Card>
                    </Col>
                </Row>
            </Aux>
             
        );
    }
    
}

export default Settings;
