import React from 'react';
import {Row, Col, Card, Table, Button, Form} from 'react-bootstrap';
import 'font-awesome/css/font-awesome.min.css';
import Aux from "../../hoc/_Aux";
import  { NavLink , Redirect} from 'react-router-dom'
import {GetData} from '../../services/GetData';

class BootstrapTable extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            data: [],
            referrer: ""
        }
     
        this.addDetails = this.addDetails.bind(this);
    }

    addDetails = () => {  
        this.setState({referrer: './addNewUser'});
          
    }

    componentDidMount() {
        GetData('users',this.state).then((result) => {
            let responseJson = result;
            this.setState({data:responseJson.data});
            });
    }

    render() {
        const {referrer} = this.state;
        if (referrer) return <Redirect to={referrer} />;
        return (
            <Aux>
                <Row>
                    <Col>
                        <Card>
                            <Card.Header>
                                {/* <Card.Title as="h5">Basic Table</Card.Title>
                                <span className="d-block m-t-5">use bootstrap <code>Table</code> component</span> */}
                            </Card.Header>
                            <Card.Body>
                                <Row>
                                    <Col md={{ size: 6 }}>
                                        <Form>
                                            <Form.Label>Status</Form.Label>
                                            <Form.Control size="small" as="select" className="mb-3">
                                                <option>Please select</option>
                                                <option>Active</option>
                                                <option>Inactive</option>
                                            </Form.Control>
                                        </Form>
                                    </Col>
                                    <Col md={{ size: 6, offset: 6}} >
                                    <input type="submit" className="btn btn-primary pull-right" value="Add" onClick={this.addDetails}  />
                                    </Col>
                                </Row>

                                <Table responsive>
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>User Name</th>
                                        <th>Email ID</th>
                                        <th>Role Type</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                   
                                    {
                                    this.state.data.map((dynamicData) =>
                                    <tr>
                                        <td>  {dynamicData.id} </td> 
                                        <td> {dynamicData.userName} </td>
                                        <td>{dynamicData.commodity}</td>
                                        <td>  {dynamicData.orderFrom} </td> 
                                        <td ><i class="fa fa-trash" aria-hidden="true" style = {{ color : '#00CCCC' }}></i> 
                                        <i class="fa fa-edit" aria-hidden="true" style = {{ color : '#00CCCC' , marginLeft : '10px'}}></i>
                                        <i class="fa fa-unlock" aria-hidden="true" style = {{ color : '#00CCCC' , marginLeft : '10px'}}></i></td>
                                    </tr>
                                    ) }
                                       
                                    </tbody>
                                </Table>
                            </Card.Body>
                        </Card>
                       
                    </Col>
                </Row>
            </Aux>
        );
    }
}

export default BootstrapTable;