import React, { Component  } from 'react';

class App extends Component {


    componentDidMount(){
        fetch('http://127.0.0.1:8000/api/test/')
        .then(function(response){
            response.json().then(resp);
            console.log(resp);
        });        
    }

    render(){
        return (
            <div className="App">
                <h1>Test</h1>
            </div>
        );
    }
}


export default App;