export default {
    items: [
        {
            id: 'navigation',
            title: 'Navigation',
            type: 'collapse',
            icon: 'icon-navigation',
            children: [
                {
                    id: 'dashboard',
                    title: 'Dashboard',
                    type: 'item',
                    url: '/dashboard/default',
                    icon: 'feather icon-home',
                }
            ]
        },
      
        {
            id: 'ui-forms',
            title: 'Forms & Tables',
            type: 'group',
            icon: 'icon-group',
            children: [
                {
                    id: 'bootstrap',
                    title: 'PO Management',
                    type: 'item',
                    icon: 'feather icon-server',
                    url: '/Po/poManagementList'
                },
                {
                    id: 'bootstrap',
                    title: 'Pattern Management',
                    type: 'item',
                    icon: 'feather icon-server',
                    url: '/Po/patternList'
                },
                {
                    id: 'bootstrap',
                    title: 'Man Process Management',
                    type: 'item',
                    icon: 'feather icon-server',
                    url: '/Po/manProcessList'
                },
                {
                    id: 'bootstrap',
                    title: 'Logistic',
                    type: 'item',
                    icon: 'feather icon-server',
                    url: '/Po/logisticList'
                },
                {
                    id: 'bootstrap',
                    title: 'Payment',
                    type: 'item',
                    icon: 'feather icon-server',
                    url: '/Po/paymentList'
                },
                
                {
                    id: 'bootstrap',
                    title: 'Quality',
                    type: 'item',
                    icon: 'feather icon-server',
                    url: '/Po/qualityList'
                },
                
               
                // ,
                // {
                //     id: 'bootstrap',
                //     title: 'RFQ Quality List',
                //     type: 'item',
                //     icon: 'feather icon-server',
                //     url: '/Po/rfqQutoesList'
                // },
                // {
                //     id: 'bootstrap',
                //     title: 'RFQ Quality Details',
                //     type: 'item',
                //     icon: 'feather icon-server',
                //     url: '/Po/rfqQutoesDetails'
                // }
            ]
        },
         
        {
            id: 'Settings',
            title: 'Settings',
            type: 'group',
            icon: 'icon-ui',
            children: [
                {
                    id: 'basic',
                    title: 'Settings',
                    type: 'collapse',
                    icon: 'feather icon-box',
                    children: [
                        {
                            id: 'bootstrap',
                            title: 'Commodity',
                            type: 'item',
                            url: '/Po/commodity',
                            icon: 'feather icon-server',
                            },
                            {
                                id: 'Order-From',
                                title: 'Order From',
                                type: 'item',
                                icon: 'feather icon-server',
                                url: '/Po/orderfrom'
                                },
                                {
                                    id: 'item-Type',
                                    title: 'Item Type',
                                    type: 'item',
                                    icon: 'feather icon-server',
                                    url: '/Po/itemType'
                                },
                                {
                                    id: 'pattern-Type',
                                    title: 'Pattern Type',
                                    type: 'item',
                                    icon: 'feather icon-server',
                                    url: '/Po/patternType'
                                },
                                {
                                    id: 'PODeliveryTerm',
                                    title: 'PO Delivery Term',
                                    type: 'item',
                                    icon: 'feather icon-server',
                                    url: '/Po/poDeliveryTerm'
                                },
                                {
                                    id: 'Payment-Term',
                                    title: 'Payment Term',
                                    type: 'item',
                                    icon: 'feather icon-server',
                                    url: '/Po/paymentTerm'
                                },
                                {
                                    id: 'status',
                                    title: 'Status',
                                    type: 'item',
                                    icon: 'feather icon-server',
                                    url: '/Po/status'
                                },
                                {
                                    id: 'ActualShippingTermsMode',
                                    title: 'Actual Shipping Terms / Mode',
                                    type: 'item',
                                    icon: 'feather icon-server',
                                    url: '/Po/actualShippingTermsMode'
                                },
                                {
                                    id: 'VendorInvoicePackingList',
                                    title: 'Vendor Invoice & Packing List',
                                    type: 'collapse',
                                    icon: 'feather icon-server',
                                    url: '/Po/vendorInvoicePackingList'
                                },
                                {
                                    id: 'CEPACertificate',
                                    title: 'CEPA Certificate',
                                    type: 'item',
                                    icon: 'feather icon-server',
                                    url: '/Po/cepaCertificate'
                                },
                                {
                                    id: 'setofQualityDocuments',
                                    title: 'Set of Quality Documents',
                                    type: 'item',
                                    icon: 'feather icon-server',
                                    url: '/Po/setofQualityDocuments'
                                },
                                {
                                    id: 'shippingModeInvoiceType',
                                    title: 'Shipping Mode / Invoice Type',
                                    type: 'item',
                                    icon: 'feather icon-server',
                                    url: '/Po/shippingModeInvoiceType'
                                },
                                {
                                    id: 'paymentResult',
                                    title: 'Payment Result',
                                    type: 'item',
                                    icon: 'feather icon-server',
                                    url: '/Po/paymentResult'
                                },
                                {
                                    id: 'finalConclusion',
                                    title: 'Final Conclusion',
                                    type: 'item',
                                    icon: 'feather icon-server',
                                    url: '/Po/finalConclusion'
                                },
                                {
                                    id: 'koreaFeedback',
                                    title: 'Korea Feedback',
                                    type: 'item',
                                    icon: 'feather icon-server',
                                    url: '/Po/koreaFeedback'
                                },
                                {
                                    id: 'rfqStatus',
                                    title: 'RFQ Status',
                                    type: 'item',
                                    icon: 'feather icon-server',
                                    url: '/Po/rfqStatus'
                                }
        
                    ]
                }
            ]
        },
        {
            id: 'ui-user',
            title: 'User',
            type: 'group',
            icon: 'icon-group',
            children: [
            {
                id: 'bootstrap',
                title: 'User',
                type: 'item',
                icon: 'feather icon-server',
                url: '/Po/userList'
            },
        ] 
    },
        
        // {
        //     id: 'chart-maps',
        //     title: 'Chart & Maps',
        //     type: 'group',
        //     icon: 'icon-charts',
        //     children: [
        //         {
        //             id: 'charts',
        //             title: 'Charts',
        //             type: 'item',
        //             icon: 'feather icon-pie-chart',
        //             url: '/charts/nvd3'
        //         },
        //         {
        //             id: 'maps',
        //             title: 'Map',
        //             type: 'item',
        //             icon: 'feather icon-map',
        //             url: '/maps/google-map'
        //         }
        //     ]
        // },
        // {
        //     id: 'pages',
        //     title: 'Pages',
        //     type: 'group',
        //     icon: 'icon-pages',
        //     children: [
        //         {
        //             id: 'auth',
        //             title: 'Authentication',
        //             type: 'collapse',
        //             icon: 'feather icon-lock',
        //             badge: {
        //                 title: 'New',
        //                 type: 'label-danger'
        //             },
        //             children: [
        //                 {
        //                     id: 'signup-1',
        //                     title: 'Sign up',
        //                     type: 'item',
        //                     url: '/auth/signup-1',
        //                     target: true,
        //                     breadcrumbs: false
        //                 },
        //                 {
        //                     id: 'signin-1',
        //                     title: 'Sign in',
        //                     type: 'item',
        //                     url: '/auth/signin-1',
        //                     target: true,
        //                     breadcrumbs: false
        //                 }
        //             ]
        //         },

        //         {
        //             id: 'sample-page',
        //             title: 'Sample Page',
        //             type: 'item',
        //             url: '/sample-page',
        //             classes: 'nav-item',
        //             icon: 'feather icon-sidebar'
        //         },
        //         {
        //             id: 'docs',
        //             title: 'Documentation',
        //             type: 'item',
        //             url: '/docs',
        //             classes: 'nav-item',
        //             icon: 'feather icon-help-circle'
        //         },
        //         {
        //             id: 'menu-level',
        //             title: 'Menu Levels',
        //             type: 'collapse',
        //             icon: 'feather icon-menu',
        //             children: [
        //                 {
        //                     id: 'menu-level-1.1',
        //                     title: 'Menu Level 1.1',
        //                     type: 'item',
        //                     url: '#!',
        //                 },
        //                 {
        //                     id: 'menu-level-1.2',
        //                     title: 'Menu Level 2.2',
        //                     type: 'collapse',
        //                     children: [
        //                         {
        //                             id: 'menu-level-2.1',
        //                             title: 'Menu Level 2.1',
        //                             type: 'item',
        //                             url: '#',
        //                         },
        //                         {
        //                             id: 'menu-level-2.2',
        //                             title: 'Menu Level 2.2',
        //                             type: 'collapse',
        //                             children: [
        //                                 {
        //                                     id: 'menu-level-3.1',
        //                                     title: 'Menu Level 3.1',
        //                                     type: 'item',
        //                                     url: '#',
        //                                 },
        //                                 {
        //                                     id: 'menu-level-3.2',
        //                                     title: 'Menu Level 3.2',
        //                                     type: 'item',
        //                                     url: '#',
        //                                 }
        //                             ]
        //                         }
        //                     ]
        //                 }
        //             ]
        //         },
                // {
                //     id: 'disabled-menu',
                //     title: 'Disabled Menu',
                //     type: 'item',
                //     url: '#',
                //     classes: 'nav-item disabled',
                //     icon: 'feather icon-power'
                // },
                /*{
                    id: 'buy-now',
                    title: 'Buy Now',
                    type: 'item',
                    icon: 'feather icon-user',
                    classes: 'nav-item',
                    url: 'https://codedthemes.com',
                    target: true,
                    external: true,
                    badge: {
                        title: 'v1.0',
                        type: 'label-primary'
                    }
                }*/
        //     ]
        // }
    ]
}