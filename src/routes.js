import React from 'react';
import $ from 'jquery';

window.jQuery = $;
window.$ = $;
global.jQuery = $;

const DashboardDefault = React.lazy(() => import('./Demo/Dashboard/Default'));

const UIBasicButton = React.lazy(() => import('./Demo/UIElements/Basic/Button'));
const UIBasicBadges = React.lazy(() => import('./Demo/UIElements/Basic/Badges'));
const UIBasicBreadcrumbPagination = React.lazy(() => import('./Demo/UIElements/Basic/BreadcrumbPagination'));

const UIBasicCollapse = React.lazy(() => import('./Demo/UIElements/Basic/Collapse'));
const UIBasicTabsPills = React.lazy(() => import('./Demo/UIElements/Basic/TabsPills'));
const UIBasicBasicTypography = React.lazy(() => import('./Demo/UIElements/Basic/Typography'));
const PoManagementDetails= React.lazy(() => import('./Demo/PO/poManagementDetails'));
const PoManagementList = React.lazy(() => import('./Demo/PO/poManagementList'));
const PatternList = React.lazy(() => import('./Demo/PO/patternList'));
const PatternDetails = React.lazy(() => import('./Demo/PO/patternDetails'));
const ManProcessList = React.lazy(() => import('./Demo/PO/manProcessList'));
const ManProcessDetails = React.lazy(() => import('./Demo/PO/manProcessDetails'));
const LogisticList = React.lazy(() => import('./Demo/PO/logisticList'));
const LogisticDetails = React.lazy(() => import('./Demo/PO/logisticDetails'));
const PaymentList = React.lazy(() => import('./Demo/PO/paymentList'));
const PaymentDetails = React.lazy(() => import('./Demo/PO/paymentDetails'));
const UserList = React.lazy(() => import('./Demo/PO/userList'));
const AddNewUser = React.lazy(() => import('./Demo/PO/addNewUser'));
const QualityList = React.lazy(() => import('./Demo/PO/qualityList'));
const QualityDetails = React.lazy(() => import('./Demo/PO/qualityDetails'));
const Commodity = React.lazy(() => import('./Demo/PO/commodity'));
const OrderFrom = React.lazy(() => import('./Demo/PO/orderFrom'));
const ItemType = React.lazy(() => import('./Demo/PO/itemType'));
const PatternType = React.lazy(() => import('./Demo/PO/patternType'));
const PODeliveryTerm = React.lazy(() => import('./Demo/PO/poDeliveryTerm'));
const PaymentTerm = React.lazy(() => import('./Demo/PO/paymentTerm'));
const Status = React.lazy(() => import('./Demo/PO/status'));
const ActualShippingTermsMode = React.lazy(() => import('./Demo/PO/actualShippingTermsMode'));
const VendorInvoicePackingList = React.lazy(() => import('./Demo/PO/vendorInvoicePackingList'));
const CEPACertificate = React.lazy(() => import('./Demo/PO/cepaCertificate'));
const SetofQualityDocuments = React.lazy(() => import('./Demo/PO/setofQualityDocuments'));
const ShippingModeInvoiceType = React.lazy(() => import('./Demo/PO/shippingModeInvoiceType'));
const PaymentResult = React.lazy(() => import('./Demo/PO/paymentResult'));
const FinalConclusion = React.lazy(() => import('./Demo/PO/finalConclusion'));
const KoreaFeedback = React.lazy(() => import('./Demo/PO/koreaFeedback'));
const RFQStatus = React.lazy(() => import('./Demo/PO/rfqStatus'));
// const RfqQutoesList = React.lazy(() => import('./Demo/PO/rfqQutoesList'));
// const RfqQutoesDetails = React.lazy(() => import('./Demo/PO/rfqQutoesDetails'));



const Nvd3Chart = React.lazy(() => import('./Demo/Charts/Nvd3Chart/index'));

const GoogleMap = React.lazy(() => import('./Demo/Maps/GoogleMap/index'));

const OtherSamplePage = React.lazy(() => import('./Demo/Other/SamplePage'));
const OtherDocs = React.lazy(() => import('./Demo/Other/Docs'));

const routes = [
    { path: '/dashboard/default', exact: true, name: 'Default', component: DashboardDefault },
    { path: '/basic/button', exact: true, name: 'Basic Button', component: UIBasicButton },
    { path: '/basic/badges', exact: true, name: 'Basic Badges', component: UIBasicBadges },
    { path: '/basic/breadcrumb-paging', exact: true, name: 'Basic Breadcrumb Pagination', component: UIBasicBreadcrumbPagination },
    { path: '/basic/collapse', exact: true, name: 'Basic Collapse', component: UIBasicCollapse },
    { path: '/basic/tabs-pills', exact: true, name: 'Basic Tabs & Pills', component: UIBasicTabsPills },
    { path: '/basic/typography', exact: true, name: 'Basic Typography', component: UIBasicBasicTypography },
    { path: '/Po/poManagementdetails', exact: true, name: 'PO Management Details', component: PoManagementDetails },
    { path: '/Po/poManagementList', exact: true, name: 'PO Management List', component: PoManagementList },
    { path: '/Po/patternList', exact: true, name: 'Pattern List', component: PatternList },
    { path: '/Po/patternDetails', exact: true, name: 'Pattern Details', component: PatternDetails },
    { path: '/Po/manProcessList', exact: true, name: 'Man Process List', component: ManProcessList },
    { path: '/Po/manProcessDetails', exact: true, name: 'Man Process List', component: ManProcessDetails },
    { path: '/Po/logisticList', exact: true, name: 'Logistic List', component: LogisticList },
    { path: '/Po/logisticDetails', exact: true, name: 'Logistic List', component: LogisticDetails },
    { path: '/Po/paymentList', exact: true, name: 'Payment List', component: PaymentList },
    { path: '/Po/paymentDetails', exact: true, name: 'Payment List', component: PaymentDetails },
    { path: '/Po/userList', exact: true, name: 'User List', component: UserList },
    { path: '/Po/addNewUser', exact: true, name: 'Add New Year', component: AddNewUser },
    { path: '/Po/qualityList', exact: true, name: 'Quality List', component: QualityList },
    { path: '/Po/qualityDetails', exact: true, name: 'Quality Details', component: QualityDetails },
    { path: '/Po/commodity', exact: true, name: 'Commodity', component: Commodity },
    { path: '/Po/orderFrom', exact: true, name: 'Order From', component: OrderFrom },
    { path: '/Po/itemType', exact: true, name: 'Item Type', component: ItemType },
    { path: '/Po/patternType', exact: true, name: 'Pattern Type', component: PatternType },
    { path: '/Po/poDeliveryTerm', exact: true, name: 'PO Delivery Term', component: PODeliveryTerm },
    { path: '/Po/paymentTerm', exact: true, name: 'Payment Term', component: PaymentTerm },
    { path: '/Po/status', exact: true, name: 'Status', component: Status },
    { path: '/Po/actualShippingTermsMode', exact: true, name: 'Status', component: ActualShippingTermsMode },
    { path: '/Po/vendorInvoicePackingList', exact: true, name: 'Vendor Invoice Packing List', component: VendorInvoicePackingList },
    { path: '/Po/cepaCertificate', exact: true, name: '  CEPA Certificate', component:  CEPACertificate },
    { path: '/Po/setofQualityDocuments', exact: true, name: 'Set of Quality Documents', component:  SetofQualityDocuments },
    { path: '/Po/shippingModeInvoiceType', exact: true, name: 'Shipping Mode Invoice Type', component:  ShippingModeInvoiceType },
    { path: '/Po/paymentResult', exact: true, name: 'Payment Result', component:  PaymentResult },
    { path: '/Po/finalConclusion', exact: true, name: 'Final Conclusion', component: FinalConclusion },
    { path: '/Po/koreaFeedback', exact: true, name: 'Korea Feedback', component: KoreaFeedback },
    { path: '/Po/rfqStatus', exact: true, name: 'RFQ Status', component: RFQStatus },
    // { path: '/Po/rfqQutoesList', exact: true, name: 'Quality List', component: RfqQutoesList },
    // { path: '/Po/rfqQutoesDetails', exact: true, name: 'Quality Details', component: RfqQutoesDetails },



    { path: '/charts/nvd3', exact: true, name: 'Nvd3 Chart', component: Nvd3Chart },
    { path: '/maps/google-map', exact: true, name: 'Google Map', component: GoogleMap },
    { path: '/sample-page', exact: true, name: 'Sample Page', component: OtherSamplePage },
    { path: '/docs', exact: true, name: 'Documentation', component: OtherDocs },
];

export default routes;