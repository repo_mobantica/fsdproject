export function GetData(type, userData) {
     let BaseURL = 'http://dev.hyosungtndindia.com/api/api/';
    //let BaseURL = 'http://localhost/dattatableapi/api/';

    return new Promise((resolve, reject) =>{
    
         
        fetch(BaseURL+type, {
            method: 'Get'
          })
          .then((response) => response.json())
          .then((res) => {
            resolve(res);
          })
          .catch((error) => {
            reject(error);
          });

  
      });
}