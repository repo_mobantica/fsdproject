export function PostData(type, userData) {
    // let BaseURL = 'https://api.thewallscript.com/restful/';
    let BaseURL = 'http://dev.hyosungtndindia.com/api/api/';
   
    return new Promise((resolve, reject) =>{
    
         
        fetch(BaseURL+type, {
            method: 'POST',
            body: JSON.stringify(userData),
            headers : { 
              'Content-Type': 'application/json',
              'Accept': 'application/json'
             }
          })
          .then((response) => response.json())
          .then((res) => { console.log("res", res);
            resolve(res);
          })
          .catch((error) => { console.log("errr", error)
            reject(error);
          });

  
      });
}